

local Fighter=require("app.ingame.Fighter")
local BigFighter=require("app.ingame.BigFighter")
local BattleControlMenu=require("app.ingame.BattleControlMenu")
local Map=require("app.ingame.Map")


local BattleScene = class("BattleScene", function()
    --    local scene = cc.Scene:createWithPhysics()
    --    scene:setNodeEventEnabled(true)
    --    scene:setAutoCleanupEnabled()
    --    scene.name="BattleScene"
    --    return scene
    return display.newScene("BattleScene",{physics=0})
end)

local rows=2
local cols=3

local battleHeight=display.height


BattleScene.stateGameInit=1
BattleScene.stateGamePlaying=BattleScene.stateGameInit+1
BattleScene.stateGameOver=BattleScene.stateGamePlaying+1

globalIngameScene=nil

function BattleScene:ctor(data)


    self.data=data
    --    self:getPhysicsWorld():setDebugDrawMask(cc.PhysicsWorld.DEBUGDRAW_ALL)
    --    self:getPhysicsWorld():setGravity(cc.p(0, 0));

    local fps={team=GTeamFriend}
    local eps={team=GTeamEnemy}

    self.map=Map.new(self)
    self:addChild(self.map)

    self.bulletLowLayer=display.newLayer()
    self:addChild(self.bulletLowLayer)

    self.fighterLayer=display.newLayer()
    self:addChild(self.fighterLayer)

    self.bulletHighLayer=display.newLayer()
    self:addChild(self.bulletHighLayer)

    self:enableNodeEvents()

    self:initGame()

    local ft=BigFighter.new(self,display.width/2,100,fps)
    self.friendBigFighter=ft
    self.fighterLayer:addChild(ft)
    --    self:add2Team(ft)

    ft=BigFighter.new(self,display.width/2,display.height-100,eps)
    --    self:add2Team(ft)
    self.fighterLayer:addChild(ft)
    self.enemyBigFighter=ft


    --    local temp={}
    --    temp[#temp+1]=cc.p(530,180)
    --    temp[#temp+1]=cc.p(108,818)
    --    temp[#temp+1]=cc.p(158,904)
    --    local action=cc.BezierTo:create(1,temp)
    --    local function callback()
    ----        fight:removeSelf()
    --        print("callback1")
    --
    --    end
    --    local delay = cc.DelayTime:create(5)
    --    local se=cc.Sequence:create({action,cc.CallFunc:create(callback)})
    --    self.friendBigFighter:runAction(se)

    local function tick(dt)
        self:updateGame(dt)
    end
    self.schedulerID=cc.Director:getInstance():getScheduler():scheduleScriptFunc(tick, 0, false)

    --    local p1 = cc.ParticleSystemQuad:create("testparticle.plist")
    --
    --    self.fighterLayer:addChild(p1)
    --    p1:setScaleY(-1)
    --    p1:setPosition(display.width/2,display.height/2)
    --    p1:setRotation(30)
    globalIngameScene=self

    self.BattleControl=BattleControlMenu.new(self)
    self:addChild(self.BattleControl)


end



function BattleScene:initGame()
    self.friends={}
    self.enemies={}
    self.readyAdds={}
    self.fightingPoints={}
    self:setGameState(BattleScene.stateGameInit)
end

function BattleScene:findFightingPoint(act1,act2)

    --    local t1x,t1y=act1:getPosition();
    --    local t2x,t2y=act2:getPosition();
    --    local midx=(t1x+t1y)/2
    --    local midy=(t2x+t2y)/2

    local x,y=self.map:findFightingPoint(act1,act2)

    return cc.p(x,y)

        --    local fightRangeX=50
        --    local fightRangeY=rowsHeight+50
        --    local fightRangeWidth=display.width-fightRangeX*2
        --    local fightRangeHeith=display.height-fightRangeY*2
        --    local randoms={}
        --    local totalRs={}
        --    local total=0
        --    for i=1,19 do
        --        local temp=i*10
        --        local tt=(10)*(i-1)*19+190
        --        if temp>100 then
        --            temp=100-(i-10)*10
        --            tt=(10)*(20-i-1)*19+190
        --        end
        --        randoms[#randoms+1]=temp
        --        total=total+tt
        --        totalRs[#totalRs+1]=total
        --
        --    end
        --    local num=math.random(1,total)
        --    local gnum=num
        ----    local num=8692
        --    local row=0
        --    local cols=0
        --    totalRs[0]=0
        --    for i=0,19 do
        --        if num>totalRs[i] and num<=totalRs[i+1] then
        --            row=i+1
        --            break;
        --        end
        --    end
        --    num=num-totalRs[row-1]
        --    cols=randoms[row]
        --    total=0
        --    if num>((randoms[row]-5)*9+randoms[row]) then
        --        num=num-((randoms[row]-5)*9+randoms[row])
        --        for i=1,9 do
        --            total=total+randoms[row]+i
        --            if num<=total then
        --                cols=i+10
        --                break;
        --            end
        --        end
        --    else
        --        for i=1,10 do
        --            total=total+randoms[row]-10+i
        --            if num<total then
        --                cols=i-1
        --                break;
        --            end
        --        end
        --    end
        --    local y=row*fightRangeHeith/19-fightRangeHeith/19/2+fightRangeY
        --    local x=cols*fightRangeWidth/19-fightRangeWidth/19/2+fightRangeX
        --
        --    return cc.p(x,y)

end

function BattleScene:caculateFlyingPoint(act,act1,act2)

    local rs={}

    return rs
end

function BattleScene:caculate1PathFromPoint(actor1,point)

    local radctr=50
    local radus=100*2
    local p1x,p1y=actor1:getPosition()

    local p2x=point.x
    local p2y=point.y
    local p1=cc.p(p1x,p1y)
    local p2=cc.p(p2x,p2y)
    --p1 起点 p2 终点
    local p1p2= cc.pSub(p2,p1)
    local angle=math.myGetAngle(p1p2)+math.random(1,10)-5
    local temp=(angle+180)*math.pi/180

    local x1=radus*math.cos(temp)
    local y1=radus*math.sin(temp)

    local dest=cc.pAdd(p2,cc.p(x1,y1))

    x1=(radus+radctr)*math.cos(temp)
    y1=(radus+radctr)*math.sin(temp)

    local fp2=cc.pAdd(p2,cc.p(x1,y1))

    temp=(angle)*math.pi/180
    x1=radctr*math.cos(temp)
    y1=radctr*math.sin(temp)

    local fp1=cc.pAdd(p1,cc.p(x1,y1))
    local rs={}
    rs[#rs+1]=p1
    rs[#rs+1]=fp1
    rs[#rs+1]=fp2
    rs[#rs+1]=dest
    return rs

end

function BattleScene:caculate1PathFromActor(act1,act2)

    local p2x,p2y=act2:getPosition()
    local p2=cc.p(p2x,p2y)
    return self:caculate1PathFromPoint(act1,p2)

end

function BattleScene:caculate2Path(act1,act2)

    local p1x,p1y=act1:getPosition()
    local p2x,p2y=act2:getPosition()
    local p1=cc.p(p1x,p1y)
    local p2=cc.p(p2x,p2y)

    local radus=100
    local radctr=150
    local da=50
    --    local mid=cc.pMidpoint(p1,p2)

    local mid=self:findFightingPoint(act1,act2)

    local v3=cc.vec3(0,0,0)
    local p1p2= cc.pSub(p2,p1)

    local angle=math.myGetAngle(p1p2)
    angle=(angle+math.random(0,da)-25)
    local temp=angle*math.pi/180
    local dx=radus*math.cos(temp)
    local dy=radus*math.sin(temp)
    local dest1=cc.pAdd(mid,cc.p(dx,dy))
    dx=radctr*math.cos(temp)
    dy=radctr*math.sin(temp)
    local destC1=cc.pAdd(mid,cc.p(dx,dy))

    angle=angle+180
    temp=angle*math.pi/180
    dx=radus*math.cos(temp)
    dy=radus*math.sin(temp)
    local dest2=cc.pAdd(mid,cc.p(dx,dy))
    dx=radctr*math.cos(temp)
    dy=radctr*math.sin(temp)
    local destC2=cc.pAdd(mid,cc.p(dx,dy))

    local dist11=getDistanceByPoint(p1,dest1)
    local dist12=getDistanceByPoint(p2,dest2)

    local dist21=getDistanceByPoint(p1,dest2)
    local dist22=getDistanceByPoint(p2,dest1)

    if (dist11+dist12)>(dist21+dist22) then
        local temp=dest2
        dest2=dest1
        dest1=temp
        temp=destC2
        destC2=destC1
        destC1=temp
    end

    p1p2=cc.pSub(dest1,p1)
    angle=math.myGetAngle(p1p2)
    angle=(angle+math.random(0,da)-25)
    temp=angle*math.pi/180
    dx=(radctr-radus)*math.cos(temp)
    dy=(radctr-radus)*math.sin(temp)
    local pC1=cc.pAdd(p1,cc.p(dx,dy))

    p1p2=cc.pSub(dest2,p2)
    angle=math.myGetAngle(p1p2)
    angle=(angle+math.random(0,da)-25)
    temp=angle*math.pi/180
    dx=(radctr-radus)*math.cos(temp)
    dy=(radctr-radus)*math.sin(temp)
    local pC2=cc.pAdd(p2,cc.p(dx,dy))

    local rs={}

    rs[#rs+1]=p1
    rs[#rs+1]=pC1
    rs[#rs+1]=destC1
    rs[#rs+1]=dest1
    rs[#rs+1]=p2
    rs[#rs+1]=pC2
    rs[#rs+1]=destC2
    rs[#rs+1]=dest2

    return rs


    

end




function BattleScene:getPositionXY(t1,t2)

    --    local tw=rowsWidth/cols
    --    local th=rowsHeight/rows
    --    local x,y
    --    local centerx=display.width/2
    --    local rr=math.floor((t2+1)/2)
    --    local cc=math.myMod(t2,2)
    --    if cc==0 then
    --        cc=2
    --    end
    --    x=centerx+rowsWidth/2-(tw/2+(rr-1)*tw)
    --    y=th/2+(cc-1)*th
    local x=FriendPositions[t2][1]
    local y=FriendPositions[t2][2]

    if t1==GTeamEnemy then
        y=battleHeight-y
    end
    return x,y
end

function BattleScene:changeBullet2High(bullet)
    bullet:retain()
    bullet:removeFromParent()
    self.bulletHighLayer:addChild(bullet)
    bullet:release()
end

function BattleScene:resetBulletTarget(target)
    for i=1,#self.bullets do
        if self.bullets[i].owner==target then
            self.bullets[i].target=nil
        end
    end
end





function BattleScene:addBullet(bullet)

    if self.bullets==nil then
        self.bullets={}
    end
    self.bullets[#self.bullets+1]=bullet
    self.bulletLowLayer:addChild(bullet)




end

function BattleScene:removeBullet(bul)

    if self.rmBullets==nil then
        self.rmBullets={}
    end
    self.rmBullets[#self.rmBullets+1]=bul

end

function BattleScene:updateBullets(dt)

    if self.bullets==nil then
        return
    end

    for i=1,#self.bullets do
        self.bullets[i]:updateBullet(dt)
    end
    local temp={}
    local flag=0
    if self.rmBullets~=nil and #self.rmBullets~=0 then
        for i=1,#self.bullets do
            flag=0
            for a=1,#self.rmBullets do
                if self.bullets[i]==self.rmBullets[a] then
                    flag=1
                    break
                end
            end
            if flag==1 then
                self.bullets[i]:removeFromParent()
            else
                temp[#temp+1]=self.bullets[i]
            end

        end
        self.rmBullets={}
        self.bullets=temp

    end



end

function BattleScene:removeFighter(act)

    if self.removeActs==nil then
        self.removeActs={}
    end
    self.removeActs[#self.removeActs+1]=act

end

function BattleScene:add2Team(act)
    self.readyAdds[#self.readyAdds+1]=act
    self.fighterLayer:addChild(act)

end


function BattleScene:updateFighters(dt)

    local temp={}
    local deads={}
    local act
    if #self.readyAdds>0 then
        for i=1,#self.readyAdds do
            act=self.readyAdds[i]
            if act.team==GTeamFriend then
                self.friends[#self.friends+1]=act
            end
            if act.team==GTeamEnemy then
                self.enemies[#self.enemies+1]=act
            end
        end
    end
    self.readyAdds={}

    if self.friends~=nil then
        for i=1,#self.friends do
            if self.friends[i].readyRemove~=1 then
                temp[#temp+1]=self.friends[i]
                self.friends[i]:updateFighter(dt)
            else
                deads[#deads+1]=self.friends[i]
            end
        end
    end
    self.friends=temp
    temp={}

    if self.enemies~=nil then
        for i=1,#self.enemies do
            if self.enemies[i].readyRemove~=1 then
                temp[#temp+1]=self.enemies[i]
                self.enemies[i]:updateFighter(dt)
            else
                deads[#deads+1]=self.enemies[i]
            end
        end
    end
    self.enemies=temp
    if #deads>0 then
        for i=1,#deads do
            deads[i]:removeFromParent()
        end
    end


end

function BattleScene:updateBigFighters(dt)

    self.friendBigFighter:updateFighter(dt)
    self.enemyBigFighter:updateFighter(dt)

end

function BattleScene:upateAllObjs(dt)

    self:updateBullets(dt)
    self:updateBigFighters(dt)
    self:updateFighters(dt)

end




function BattleScene:setGameState(sta)

    self.state=sta
end

function BattleScene:updateGame(dt)

    --    self:getPhysicsWorld():setAutoStep(false)
    --    self:getPhysicsWorld():step(1/30);



    if self.state==BattleScene.stateGameInit then

        self:setGameState(BattleScene.stateGamePlaying)

    elseif self.state==BattleScene.stateGamePlaying then
        self.map:updateCurTiles()
        self:upateAllObjs(dt)

    elseif self.state==BattleScene.stateGameOver then

    end



end



function BattleScene:onEnter()


end

function BattleScene:onExit()


end

function BattleScene:onEnterTransitionFinish()


end

function BattleScene:onExitTransitionStart()


end

function BattleScene:onCleanup()

    cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self.schedulerID)
    globalIngameScene=nil
end


return BattleScene