
local TextureNode = class("TextureNode", function()
    local glNode  = gl.glNodeCreate()
    return glNode;
end)

function TextureNode:ctor(parent,x,y,tx,ty,width)
    self.parent=parent
    self.x=x
    self.y=y
    self.tx=tx
    self.ty=ty
    self.width=width
    
--    local array={cc.p(256,0+50),cc.p(256+100,200+50),cc.p(256-100,400+50),cc.p(256+100,600+50),cc.p(256,800+50)}
    
    self.node1=display.newNode()
    self.node2=display.newNode()
    self.node3=display.newNode()
    
    self.parent:addChild(self.node1)
    self.parent:addChild(self.node2)
    self.parent:addChild(self.node3)
    local offx=3
    
    self.node1:setPosition(cc.p(256+offx,200+50))
    self.node2:setPosition(cc.p(256-offx,400+50))
    self.node3:setPosition(cc.p(256+offx,600+50))
    
    local time=0.5
    local action1=cc.MoveTo:create(time,cc.p(256-offx,250))
    local action2=cc.MoveTo:create(time*2,cc.p(256+offx,250))
    local seq=cc.Sequence:create(action1,action2)
    self.node1:runAction(cc.RepeatForever:create(seq))
    
    action1=cc.MoveTo:create(time,cc.p(256+offx,450))
    action2=cc.MoveTo:create(time*2,cc.p(256-offx,450))
    seq=cc.Sequence:create(action1,action2)
    self.node2:runAction(cc.RepeatForever:create(seq))
    
    action1=cc.MoveTo:create(time,cc.p(256-offx,650))
    action2=cc.MoveTo:create(time*2,cc.p(256+offx,650))
    seq=cc.Sequence:create(action1,action2)
    self.node3:runAction(cc.RepeatForever:create(seq))
    
    self.glBuffers={}

    self.texture=cc.Director:getInstance():getTextureCache():addImage("beam1.png")

    self.glBuffers.vertexBuffer=gl.createBuffer()
    self.glBuffers.texBuffer=gl.createBuffer()

    local hh=self.texture:getPixelsHigh()
    local v1=cc.vec3(tx-x,ty-y,0)
    local v2=cc.vec3(0,0,1)
    local dst=cc.vec3(0,0,0)
    dst=vec3_cross(v1,v2,dst)
    dst=cc.vec3normalize(dst)
    dst=cc.p(dst.x*self.width,dst.y*self.width)
    self.org=cc.p(self.x,self.y)
    self.tp=cc.p(self.tx,self.ty)

    local len=cc.pGetDistance(self.org,self.tp)
    local start=0

    local shader = cc.ShaderCache:getInstance():getGLProgram("ShaderPositionTexture")
    gl.bindBuffer(gl.ARRAY_BUFFER,0)

    local function glLinedraw(transform, transformUpdated)
        if nil ~= shader then
            shader:use()
            shader:setUniformsForBuiltins(transform)
            gl.bindTexture(gl.TEXTURE_2D, self.texture:getName())
            start=start+0.05

            if start>=1 then
                start=0
            end
            local temp=0
            local th=1-start
            local tpp
            local p1=cc.pAdd(self.org,cc.p(-dst.x,-dst.y))
            local p2=cc.pAdd(self.org,cc.p(dst.x,dst.y))
            local p3
            local p4
            local vertices={}
            local texcoords={}
            local count=0
            local tt=0
            while temp<len do
                temp=hh*th
                count=count+6
                if temp>=len then
                    if th>=1 then
                        tt=1-(temp-len)/hh
                    else
                        tt=(len)/hh+start
                    end
                    p3=cc.pAdd(self.tp,cc.p(dst.x,dst.y))
                    p4=cc.pAdd(self.tp,cc.p(-dst.x,-dst.y))
                else
                    tt=1
                    tpp=cc.p(temp*(self.tx-self.x)/len+self.x,temp*(self.ty-self.y)/len+self.y)
                    p3=cc.pAdd(tpp,cc.p(dst.x,dst.y))
                    p4=cc.pAdd(tpp,cc.p(-dst.x,-dst.y))
                end
                vertices[#vertices+1]=p1.x
                vertices[#vertices+1]=p1.y
                vertices[#vertices+1]=p2.x
                vertices[#vertices+1]=p2.y
                vertices[#vertices+1]=p3.x
                vertices[#vertices+1]=p3.y
                vertices[#vertices+1]=p1.x
                vertices[#vertices+1]=p1.y
                vertices[#vertices+1]=p3.x
                vertices[#vertices+1]=p3.y
                vertices[#vertices+1]=p4.x
                vertices[#vertices+1]=p4.y
                if th>=1 then
                    texcoords[#texcoords+1]=0
                    texcoords[#texcoords+1]=1
                    texcoords[#texcoords+1]=1
                    texcoords[#texcoords+1]=1
                    texcoords[#texcoords+1]=1
                    texcoords[#texcoords+1]=1-tt
                    texcoords[#texcoords+1]=0
                    texcoords[#texcoords+1]=1
                    texcoords[#texcoords+1]=1
                    texcoords[#texcoords+1]=1-tt
                    texcoords[#texcoords+1]=0
                    texcoords[#texcoords+1]=1-tt
                else
                    texcoords[#texcoords+1]=0
                    texcoords[#texcoords+1]=1-start
                    texcoords[#texcoords+1]=1
                    texcoords[#texcoords+1]=1-start
                    texcoords[#texcoords+1]=1
                    texcoords[#texcoords+1]=1-tt
                    texcoords[#texcoords+1]=0
                    texcoords[#texcoords+1]=1-start
                    texcoords[#texcoords+1]=1
                    texcoords[#texcoords+1]=1-tt
                    texcoords[#texcoords+1]=0
                    texcoords[#texcoords+1]=1-tt
                end
                th=th+1
                p1=p4
                p2=p3
            end

            gl.glEnableVertexAttribs(bit._or(cc.VERTEX_ATTRIB_FLAG_TEX_COORDS, cc.VERTEX_ATTRIB_FLAG_POSITION))

            gl.bindBuffer(gl.ARRAY_BUFFER, self.glBuffers.vertexBuffer)
            gl.bufferData(gl.ARRAY_BUFFER,table.getn(vertices),vertices,gl.DYNAMIC_DRAW)
            gl.vertexAttribPointer(cc.VERTEX_ATTRIB_POSITION,2,gl.FLOAT,false,0,0)

            gl.bindBuffer(gl.ARRAY_BUFFER, self.glBuffers.texBuffer)
            gl.bufferData(gl.ARRAY_BUFFER,table.getn(texcoords),texcoords,gl.DYNAMIC_DRAW)
            gl.vertexAttribPointer(cc.VERTEX_ATTRIB_TEX_COORD,2,gl.FLOAT,false,0,0)

            --            gl.bindBuffer(gl.ARRAY_BUFFER, self.glBuffers.vertexBuffer)
            --            gl.vertexAttribPointer(cc.VERTEX_ATTRIB_POSITION,2,gl.FLOAT,false,0,0)
            --
            --            gl.bindBuffer(gl.ARRAY_BUFFER, self.glBuffers.texBuffer)
            --            gl.vertexAttribPointer(cc.VERTEX_ATTRIB_TEX_COORD,2,gl.FLOAT,false,0,0)

            gl.drawArrays(gl.TRIANGLES,0,count)

            gl.bindTexture(gl.TEXTURE_2D,0)
            gl.bindBuffer(gl.ARRAY_BUFFER,0)

        end

    end

--    local array={cc.p(256,0+50),cc.p(256+100,200+50),cc.p(256-100,400+50),cc.p(256+100,600+50),cc.p(256,800+50)}
    
    
    local stre=0
    local tension=0
    local normal
    
    local testFlag=0
    local offset=0
    local function glCurvedraw(transform, transformUpdated)

        local array={cc.p(256,0+50)}
        local tx,ty=self.node1:getPosition()
        array[#array+1]=cc.p(tx,ty)
        tx,ty=self.node2:getPosition()
        array[#array+1]=cc.p(tx,ty)
        tx,ty=self.node3:getPosition()
        array[#array+1]=cc.p(tx,ty)
        array[#array+1]=cc.p(256,800+50)
        if nil ~= shader then
            local midPoints={array[1]}
        
            shader:use()
            shader:setUniformsForBuiltins(transform)
            gl.bindTexture(gl.TEXTURE_2D, self.texture:getName())

            local time=0
            local delta=1/(#array-1)
            local index
            local vertexs={}
            
            local texs={}
            offset=offset+0.02
            if offset>=1 then
                offset=0
            end
            start=offset
            local v1
            local v2
            local v3
            local v4
            local pp
            local vv1
            local vv2=cc.vec3(0,0,1)
            local dist
            local temp
            local mv1
            local mv2
            local mv3
            local mv4
            local normal=cc.vec3(0,0,0)
            local first=1
            local time1
            
            while time<1 do
                time=time+0.01
                index=math.floor(time/delta)+1
                
                if first==1 then
                    first=0
                    v1=self:getPointFromArray(array,index-1)
                    v2=self:getPointFromArray(array,index)
                    v3=self:getPointFromArray(array,index+1)
                    v4=self:getPointFromArray(array,index+2)
                    pp=self:ccCardinalSplineAt(v1, v2, v3, v4, tension, (time-math.floor(time/delta)*delta)/delta)
                    vv1=cc.pSub(pp,array[1])

                    normal=vec3_cross(cc.vec3(vv1.x,vv1.y,0) ,vv2,normal)
                    normal=cc.vec3normalize(normal)
                    normal=cc.p(normal.x*self.width,normal.y*self.width)

                    dist=cc.pGetDistance(pp,midPoints[#midPoints])
                    mv1=cc.pAdd(midPoints[#midPoints],normal)
                    mv2=cc.pAdd(midPoints[#midPoints], self:reverseVec2(normal))
                else
                    time1=(time-math.floor(time/delta)*delta)/delta
                    if time>=1 then
                        index=#array
                        time1=1
                    end

                    v1=self:getPointFromArray(array,index-1)
                    v2=self:getPointFromArray(array,index)
                    v3=self:getPointFromArray(array,index+1)
                    v4=self:getPointFromArray(array,index+2)
                    pp=self:ccCardinalSplineAt(v1, v2, v3, v4, tension,time1) 
                    vv1=cc.pSub(pp,midPoints[#midPoints])
                    normal=vec3_cross(cc.vec3(vv1.x,vv1.y,0) ,vv2,normal)
                    normal=cc.vec3normalize(normal)
                    normal=cc.p(normal.x*self.width,normal.y*self.width)
                    dist=cc.pGetDistance(pp,midPoints[#midPoints])

                end
--                print("normal",normal.x,normal.y,pp.x,pp.y)
                if dist>hh*(1-start) then
                    temp=hh*(1-start)
                    local tpp=temp/dist
                    local temp1=midPoints[#midPoints]
                    tpp=cc.p((pp.x-temp1.x)*tpp+temp1.x,(pp.y-temp1.y)*tpp+temp1.y)

                    mv3=cc.pAdd(tpp,self:reverseVec2(normal))
                    mv4=cc.pAdd(tpp,normal)

                    vertexs[#vertexs+1]=mv1.x
                    vertexs[#vertexs+1]=mv1.y

                    vertexs[#vertexs+1]=mv2.x
                    vertexs[#vertexs+1]=mv2.y

                    vertexs[#vertexs+1]=mv3.x
                    vertexs[#vertexs+1]=mv3.y

                    texs[#texs+1]=1
                    texs[#texs+1]=1-start
                    texs[#texs+1]=0
                    texs[#texs+1]=1-start
                    texs[#texs+1]=0
                    texs[#texs+1]=0

                    vertexs[#vertexs+1]=mv3.x
                    vertexs[#vertexs+1]=mv3.y

                    vertexs[#vertexs+1]=mv4.x
                    vertexs[#vertexs+1]=mv4.y

                    vertexs[#vertexs+1]=mv1.x
                    vertexs[#vertexs+1]=mv1.y

                    texs[#texs+1]=0
                    texs[#texs+1]=0
                    texs[#texs+1]=1
                    texs[#texs+1]=0
                    texs[#texs+1]=1
                    texs[#texs+1]=1-start
                    --other
                    local endtext=(dist-hh*(1-start))/hh
                    mv1=mv4
                    mv2=mv3
                    mv3=cc.pAdd(pp,self:reverseVec2(normal))
                    mv4=cc.pAdd(pp,normal)

                    vertexs[#vertexs+1]=mv1.x
                    vertexs[#vertexs+1]=mv1.y

                    vertexs[#vertexs+1]=mv2.x
                    vertexs[#vertexs+1]=mv2.y

                    vertexs[#vertexs+1]=mv3.x
                    vertexs[#vertexs+1]=mv3.y

                    texs[#texs+1]=1
                    texs[#texs+1]=1
                    texs[#texs+1]=0
                    texs[#texs+1]=1
                    texs[#texs+1]=0
                    texs[#texs+1]=1-endtext

                    vertexs[#vertexs+1]=mv3.x
                    vertexs[#vertexs+1]=mv3.y

                    vertexs[#vertexs+1]=mv4.x
                    vertexs[#vertexs+1]=mv4.y

                    vertexs[#vertexs+1]=mv1.x
                    vertexs[#vertexs+1]=mv1.y

                    texs[#texs+1]=0
                    texs[#texs+1]=1-endtext
                    texs[#texs+1]=1
                    texs[#texs+1]=1-endtext
                    texs[#texs+1]=1
                    texs[#texs+1]=1

                    mv1=mv4
                    mv2=mv3
                    start=endtext

                else
                    temp=hh*(1-start)-dist
                    temp=temp/hh
                    mv3=cc.pAdd(pp,self:reverseVec2(normal))
                    mv4=cc.pAdd(pp,normal)

                    vertexs[#vertexs+1]=mv1.x
                    vertexs[#vertexs+1]=mv1.y

                    vertexs[#vertexs+1]=mv2.x
                    vertexs[#vertexs+1]=mv2.y

                    vertexs[#vertexs+1]=mv3.x
                    vertexs[#vertexs+1]=mv3.y

                    texs[#texs+1]=1
                    texs[#texs+1]=1-start
                    texs[#texs+1]=0
                    texs[#texs+1]=1-start
                    texs[#texs+1]=0
                    texs[#texs+1]=temp

                    vertexs[#vertexs+1]=mv3.x
                    vertexs[#vertexs+1]=mv3.y

                    vertexs[#vertexs+1]=mv4.x
                    vertexs[#vertexs+1]=mv4.y

                    vertexs[#vertexs+1]=mv1.x
                    vertexs[#vertexs+1]=mv1.y

                    texs[#texs+1]=0
                    texs[#texs+1]=temp
                    texs[#texs+1]=1
                    texs[#texs+1]=temp
                    texs[#texs+1]=1
                    texs[#texs+1]=1-start

                    mv1=mv4
                    mv2=mv3
                    start=1-temp
                end
                midPoints[#midPoints+1]=pp

            end
            gl.glEnableVertexAttribs(bit._or(cc.VERTEX_ATTRIB_FLAG_TEX_COORDS, cc.VERTEX_ATTRIB_FLAG_POSITION))

            gl.bindBuffer(gl.ARRAY_BUFFER, self.glBuffers.vertexBuffer)
            gl.bufferData(gl.ARRAY_BUFFER,table.getn(vertexs),vertexs,gl.DYNAMIC_DRAW)
            gl.vertexAttribPointer(cc.VERTEX_ATTRIB_POSITION,2,gl.FLOAT,false,0,0)

            gl.bindBuffer(gl.ARRAY_BUFFER, self.glBuffers.texBuffer)
            gl.bufferData(gl.ARRAY_BUFFER,table.getn(texs),texs,gl.DYNAMIC_DRAW)
            gl.vertexAttribPointer(cc.VERTEX_ATTRIB_TEX_COORD,2,gl.FLOAT,false,0,0)


            gl.drawArrays(gl.TRIANGLES,0,#vertexs/2)

            gl.bindTexture(gl.TEXTURE_2D,0)
            gl.bindBuffer(gl.ARRAY_BUFFER,0)


        end


    end


    self:registerScriptDrawHandler(glCurvedraw)
end

function TextureNode:ccCardinalSplineAt(p0, p1, p2, p3, tension, t)

    local t2 = t * t;
    local t3 = t2 * t;

    --    /*
    --     * Formula: s(-ttt + 2tt - t)P1 + s(-ttt + tt)P2 + (2ttt - 3tt + 1)P2 + s(ttt - 2tt + t)P3 + (-2ttt + 3tt)P3 + s(ttt - tt)P4
    --     */
    local s = (1 - tension) / 2;

    local b1 = s * ((-t3 + (2 * t2)) - t);                      --// s(-t3 + 2 t2 - t)P1
    local b2 = s * (-t3 + t2) + (2 * t3 - 3 * t2 + 1);          --// s(-t3 + t2)P2 + (2 t3 - 3 t2 + 1)P2
    local b3 = s * (t3 - 2 * t2 + t) + (-2 * t3 + 3 * t2);     -- // s(t3 - 2 t2 + t)P3 + (-2 t3 + 3 t2)P3
    local b4 = s * (t3 - t2);                                   --// s(t3 - t2)P4

    local x = (p0.x*b1 + p1.x*b2 + p2.x*b3 + p3.x*b4);
    local y = (p0.y*b1 + p1.y*b2 + p2.y*b3 + p3.y*b4);

    return cc.p(x,y)
end

function TextureNode:reverseVec2(normal)

    return cc.p(-normal.x,-normal.y)
end

function TextureNode:getPointFromArray(array,index)
    if index<1 then
        index=1
    end
    if index>#array then
        index=#array
    end
    return array[index]
end




function TextureNode:onEnter()


end

function TextureNode:onExit()


end

function TextureNode:onEnterTransitionFinish()


end

function TextureNode:onExitTransitionStart()


end

function TextureNode:onCleanup()
    gl.deleteBuffer(self.glBuffers.texBuffer)
    gl.deleteBuffer(self.glBuffers.vertexBuffer)
end


return TextureNode