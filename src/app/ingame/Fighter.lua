
local Fighter = class("Fighter", function(parent,x,y,owner)

        return display.newNode()
end)


local Bullet=require("app.ingame.Bullet")


Fighter.StateInit=1

Fighter.StateSpawing=Fighter.StateInit+1


Fighter.StateTargetFinding=Fighter.StateSpawing+1
Fighter.StateTargetFlying=Fighter.StateTargetFinding+1
Fighter.StateTargetWaiting=Fighter.StateTargetFlying+1
Fighter.StateTargetAttackingInit=Fighter.StateTargetWaiting+1
Fighter.StateTargetAttacking=Fighter.StateTargetAttackingInit+1

Fighter.StateDying=Fighter.StateTargetAttacking+1
Fighter.StateDead=Fighter.StateDying+1


function Fighter:ctor(parent,x,y,pros,owner)
    GID=GID+1
    self.gID=GID
    self.orgX=x
    self.orgY=y
    self.x=x
    self.y=y
    self.oldX=x
    self.oldY=y

    self.pros=pros
    self.gameScene=parent
    self.team=owner.team
    self.moveSpeed=100
    self.moveRange=50
    self.moveDest=nil
    self.type=GFighterTypeSmall
    self.owner=owner
    self.hp=tonumber(self.pros.hp)
    local sprite=display.newSprite()

    local temp=self.pros.resID
    local csb="fighter_"..temp..".csb"

    local node=cc.CSLoader:createNode(csb)
    sprite:addChild(node)
    self.csbNode=node
    self.fire1=node:getChildByName("fire1")
    self.fire2=node:getChildByName("fire2")

    self.emitter1=node:getChildByName("emitter1")
    self.emitter2=node:getChildByName("emitter2")
    local p1 = cc.ParticleSystemQuad:create("testparticle.plist")
    local p2 = cc.ParticleSystemQuad:create("testparticle.plist")
    self.emitter1:addChild(p1)
    self.emitter2:addChild(p2)
    sprite:setScale(0.5)
    self.sprite=sprite
    if self.team==GTeamEnemy then
        sprite:setScaleY(sprite:getScaleY()*-1)
    end

    --    self:initPhysic()
    self:initFighter()
    self:setPosition(self.x,self.y)

    self:addChild(sprite)

--    local  label = cc.Label:createWithSystemFont("g"..self.gID,"Arial", 38)
--    label:setAnchorPoint(cc.p(0.5, 0.5))
--    self:addChild(label)

end

function Fighter:initFighter()
    self.state=Fighter.StateInit
    self.bulletDelta=0.5
    self.lastFireTime=-0.5
    self.accuTime=0

    self:resetFightPos()
    --    if self.team==GTeamEnemy then
    --        self.fightPos=self.gameScene.map.FriPos
    --    else
    --        self.fightPos=self.gameScene.map.EnePos
    --    end
end


function Fighter:initPhysic()
    self:setAnchorPoint(cc.p(0.5, 0.5))
    self:setPhysicsBody(cc.PhysicsBody:createCircle(100*self.sprite:getScaleX()));
    self:getPhysicsBody():setContactTestBitmask(1);
    --    setCategoryBitmask(4);
    self:getPhysicsBody():setCollisionBitmask(0);

end


function Fighter:isReachDest()
    if self.desX==nil or self.desY==nil then
        return true
    end

    if self.x==self.desX and self.y==self.desY then
        return true
    end

    return false

end


function Fighter:updatePosition(dt)

    local dx=self.desX
    local dy=self.desY
    local delX=dx-self.x
    local delY=dy-self.y
    if delX==0 and delY==0 then
        return
    end

    local sq=math.sqrt(delX*delX+delY*delY)
    --    print("dx,dy",dx,dy)
    local ty=delY*self.moveSpeed*dt/sq
    local tx=delX*self.moveSpeed*dt/sq
    self.oldX=self.x
    self.oldY=self.y
    self.x=self.x+tx
    self.y=self.y+ty

    local tx1=self.oldX
    local tx2=self.x
    local ty1=self.oldY
    local ty2=self.y
    if self.oldX>self.x then
        tx1=self.x
        tx2=self.oldX
    end
    if self.oldY>self.y then
        ty1=self.y
        ty2=self.oldY
    end
    if tx1<=dx and tx2>=dx and ty1<=dy and ty2>=dy then
        self.x=dx
        self.y=dy
    end
    --        print("x,Y",self.id,self.x,self.y,self:isFlippedX())
    self:setPosition(cc.p(self.x,self.y))

end

function Fighter:stopFlyingAction()

    if self.flyingAction~=nil then
        --        self:stopAllActions()
        self:stopAction(self.flyingAction)
        self.flyingAction=nil
    end

end

function Fighter:setFighterState(sta)

    if sta==Fighter.StateSpawing then
        self:setSpawing()

    elseif sta==Fighter.StateTargetFinding then
        self:resetFightPos()

    elseif sta==Fighter.StateTargetFlying then
        local function callback()
            self:setFighterState(Fighter.StateTargetAttackingInit)
        end
        --        for i=1,#self.flyingPath do
        --            print("Gid",self.gID,self.flyingPath[i].x,self.flyingPath[i].y)
        --        end
        --
        local action=cc.Sequence:create(cc.BezierTo:create(1,self.flyingPath),cc.CallFunc:create(callback))
        self:setFlyinyAction(action)
        self:runAction(self.flyingAction)
    elseif sta==Fighter.StateTargetAttackingInit then


    elseif sta==Fighter.StateDying then
        self.gameScene:resetBulletTarget(self)

    elseif self.state==Fighter.StateDead then

        self:removeSelf()

    end

    self.state=sta

end

function Fighter:findNormalMoveDest()

    local x=self.moveRange-math.random(self.moveRange*2)
    self.desX=self.orgX+x
    local y=self.moveRange-math.random(self.moveRange*2)
    self.desY=self.orgY+y



end


function Fighter:findOneEnemy()
    local fs
    if self.team==GTeamEnemy then
        fs=self.gameScene.friends
    else
        fs=self.gameScene.enemies
    end
    local index=-1
    local best=-1
    for i=1,#fs do
        if i==1 then
            index=i
            best=getDistance(self.x,self.y,fs[i].x,fs[i].y)
        else
            local d=getDistance(self.x,self.y,fs[i].x,fs[i].y)
            if best>d then
                index=i
                best=d
            end
        end
    end
    if index>0 then
        return fs[index]
    end
    return nil

end

function Fighter:checkPaired()
    if self.target~=nil and self.target.target==self then
        return 1
    end

    return 0
end


function Fighter:findTarget()
    local targets
    if self.team==GTeamEnemy then
        targets=self.gameScene.friends
    else
        targets=self.gameScene.enemies
    end

    local pairs={}
    local unpairs={}
    for i=1,#targets do
        if targets[i]:isLive() ==1 then
            if targets[i]:checkPaired()==1 then
                pairs[#pairs+1]=targets[i]
            else
                unpairs[#unpairs+1]=targets[i]
            end
        end

    end

    if #unpairs>0 then
        targets=unpairs
    else
        targets=pairs
    end

    local temp={}
    local pri={}
    for i=1,#targets do
        if targets[i]:isAtting()==1 then
            temp[#temp+1]=targets[i]
            if targets[i].target==self then
                pri[#pri+1]=targets[i]
            end
        elseif targets[i]:isTargetFlying()==1 then
            table.insert(temp,1,targets[i])
            if targets[i].target==self then
                table.insert(pri,1,targets[i])
            end
        end

    end
    local target=nil

    if self.team==GTeamEnemy then
        target=self.gameScene.friendBigFighter
    else
        target=self.gameScene.enemyBigFighter
    end

    if #temp>0 then
        target= temp[1]
    end

    if #pri>0 then
        target= pri[1]
    end
    return target
end

function Fighter:setTarget(target)

    self.target=target

end




function Fighter:willDodge()

    return false
end

function Fighter:spawBullet(bb)

    self.gameScene:addBullet(bb)

end

function Fighter:fire(dt)
    local pros=weaponsData[1]
    if self.team==GTeamEnemy then
        pros=weaponsData[2]
    end

    --    local bullpros1={fire=1,type=tt}
    --    local bullpros2={fire=2,type=tt}
    local bb1=Bullet.new(self.gameScene,self,self.target,pros,1)
    local bb2=Bullet.new(self.gameScene,self,self.target,pros,2)

    self:spawBullet(bb1)
    self:spawBullet(bb2)



end

function Fighter:updateFire(dt)

    if self.accuTime-self.lastFireTime<self.bulletDelta then
        return
    end

    self.lastFireTime=self.accuTime
    self:fire(dt)
end


function Fighter:updateNormalFlying(dt)

    if self:isReachDest() then
        self:findNormalMoveDest()
    end
    self:updatePosition(dt)

end


function Fighter:setSpawing()

    local rad=100

    local angle=math.pi*math.random(30,150)/180
    if self.team==GTeamEnemy then
        angle=math.pi*math.random(210,330)/180
    end


    local x=rad*math.cos(angle)
    local y=rad*math.sin(angle)
    local dx=self.orgX+x
    local dy=self.orgY+y
    local action=cc.MoveTo:create(0.2,cc.p(dx,dy))

    local function callback()
        self:setFighterState(Fighter.StateTargetFinding)
    end

    local sequence = cc.Sequence:create(action, cc.CallFunc:create(callback))
    self:runAction(sequence)


end

function Fighter:updateFlying(dt)


    self.x,self.y=self:getPosition()
    if self.x==self.oldX and self.y==self.oldY then

    else
        local angle=math.myGetAngle(cc.p(self.x-self.oldX,self.y-self.oldY))
        if self.team==GTeamEnemy then
            angle=angle-180
        end
        self:setRotation(90-angle)
    end
    self.oldX=self.x
    self.oldY=self.y

end

function Fighter:isDead()
    if self.state==Fighter.StateDying or self.state==Fighter.StateDead then
        return 1
    end
    return 0
end

function Fighter:isLive()
    if self:isDead()==1 then
        return 0
    end
    return 1

end

function Fighter:isAtted()

    local targets
    if self.team==GTeamEnemy then
        targets=self.gameScene.friends
    else
        targets=self.gameScene.enemies
    end

    for i=1,#targets do
        if targets[i].target==self and (targets[i]:isAtting()==1) then
            return 1
        end
    end
    return 0

end

function Fighter:isAtting()
    if self.state==Fighter.StateTargetAttacking or self.state==Fighter.StateTargetAttackingInit then
        return 1
    end
    return 0
end

function Fighter:isAimed()
    local targets
    if self.team==GTeamEnemy then
        targets=self.gameScene.friends
    else
        targets=self.gameScene.enemies
    end

    for i=1,#targets do
        if targets[i].target==self and (targets[i].state==Fighter.StateTargetFlying) then
            return 1
        end
    end
    return 0

end



function Fighter:checkTargetDone()
    if self.target==nil then
        return 1
    else
        if self.target:isDead()==1 then
            return 1
        end
    end
    return 0
end

function Fighter:isInAttRange(target)


    return 0
end

function Fighter:isTargetFlying()
    if self.state==Fighter.StateTargetFlying then
        return 1
    end
    return 0
end

function Fighter:setFlyingPath(path)
    self.flyingPath=path

end

function Fighter:setFlyinyAction(action)
    self.flyingAction=action
end

function Fighter:getFlyingDest()

    if self:isTargetFlying()==1 then
        return self.flyingPath[3]
    end
    return nil

end

function Fighter:face2Target()

    local p1=cc.p(self:getPosition())
    local p2=cc.p(self.target:getPosition())
    local p1p2= cc.pSub(p2,p1)

    local angle=math.myGetAngle(p1p2)
    if self.team==GTeamEnemy then
        angle=angle-180
    end

    self:setRotation(90-angle)

end

function Fighter:set1Flying(target)

    local array=self.gameScene:caculate1PathFromActor(self,target)
    local temp={}
    temp[#temp+1]=array[2]
    temp[#temp+1]=array[3]
    temp[#temp+1]=array[4]
    self:setFlyingPath(temp)
    self:setFighterState(Fighter.StateTargetFlying)

    local x,y=target:getPosition()
    local p=cc.pMidpoint(cc.p(x,y),array[4])
    x,y=self.gameScene.map:getTileAt(p.x,p.y)
    self:setFightPos(cc.p(x,y))
    if target.type==GFighterTypeBig then
        self:resetFightPos()
    end

end

function Fighter:set2Flying(target)

    local array=self.gameScene:caculate2Path(self,target)
    local temp={}
    temp[#temp+1]=array[2]
    temp[#temp+1]=array[3]
    temp[#temp+1]=array[4]
    self:setFlyingPath(temp)
    temp={}
    temp[#temp+1]=array[6]
    temp[#temp+1]=array[7]
    temp[#temp+1]=array[8]
    target:setFlyingPath(temp)

    self:setFighterState(Fighter.StateTargetFlying)
    target:setFighterState(Fighter.StateTargetFlying)

    local p=cc.pMidpoint(self:getFlyingDest(),target:getFlyingDest())
    local x,y=self.gameScene.map:getTileAt(p.x,p.y)
    self:setFightPos(cc.p(x,y))
    target:setFightPos(cc.p(x,y))
end

function Fighter:getAimedTargets()

    local targets
    if self.team==GTeamEnemy then
        targets=self.gameScene.friends
    else
        targets=self.gameScene.enemies
    end
    local temp={}

    for i=1,#targets do
        if targets[i].target==self and self.target~=targets[i].target then
            temp[#temp+1]=targets[i]
        end
    end
    if #temp>1 then
        return temp
    end

    return nil

end

function Fighter:stopAttackFlyingAction()
    if self.attackFlyingAction~=nil then
        self:stopAction(self.attackFlyingAction)
        self.attackFlyingAction=nil
    end

end

function Fighter:hurt(bullet)

    if self.hp<=0 then
        self.hp=0

    end
    self.hp=self.hp-bullet.attack
    if self.hp<0 then
        self.hp=0
        self:setFighterState(Fighter.StateDying)
    end


end

function Fighter:setAttackFlying()

    local function moveFunc()
        local x=self.attX+math.random(2,15)-30
        local y=self.attY+math.random(2,15)-30
        local tx,ty=self:getPosition()
        local dis=cc.pGetDistance(cc.p(x,y),cc.p(tx,ty))

        local time=dis/10*1
        local action=cc.MoveTo:create(time,cc.p(x,y))

        --        local action = cc.OrbitCamera:create(2,1, 0, 5, 0, 0, 360)
        --        local action=cc.JumpTo:create(1, cc.p(x,y), 20, 1)

        local seq=cc.Sequence:create(action,cc.CallFunc:create(moveFunc))
        self.attackFlyingAction=seq
        self:runAction(seq)

    end
    self.attackFlyingAction=cc.CallFunc:create(moveFunc)
    self:runAction(self.attackFlyingAction)

end

function Fighter:resetFightPos()
    if self.team==GTeamEnemy then
        self:setFightPos(self.gameScene.map.FriPos)
    else
        self:setFightPos(self.gameScene.map.EnePos)
    end
end

function Fighter:setFightPosByAct(act)

    if act.type==GFighterTypeBig then
        self:resetFightPos()
        return
    end

    local a1x,a1y=self:getPosition()
    local a2x,a2y=act:getPosition()
    local midx,midy=cc.p((a1x+a2x)/2,(a1y+a2y)/2)
    local tx,ty=self.gameScene.map:getTileAt(midx,midy)

    self:setFightPos(cc.p(tx,ty))

end

function Fighter:setFightPos(pos)
    self.fightPos=pos

    if pos.x==-1  then
        self:resetFightPos()
    end
end

function Fighter:printActorState()
    local ss=self.gID.." "
    if self.state==Fighter.StateInit then
        ss=ss.."Fighter.StateInit"
    elseif self.state==Fighter.StateSpawing then
        ss=ss.."Fighter.StateSpawing"
    elseif self.state==Fighter.StateTargetFinding then
        ss=ss.."Fighter.StateTargetFinding"
    elseif self.state==Fighter.StateTargetFlying then
        ss=ss.."Fighter.StateTargetFlying"
    elseif self.state==Fighter.StateTargetWaiting then
        ss=ss.."Fighter.StateTargetWaiting"
    elseif self.state==Fighter.StateTargetAttackingInit then
        ss=ss.."Fighter.StateTargetAttackingInit"
    elseif self.state==Fighter.StateTargetAttacking then
        ss=ss.."Fighter.StateTargetAttacking"
    elseif self.state==Fighter.StateDead then
        ss=ss.."Fighter.StateDead"

    end
    if self.target~=nil then
        ss=ss.." target:"..self.target.gID
    end

    print(ss)
end

function Fighter:updateFighter(dt)

    self.accuTime=self.accuTime+dt

--    self:printActorState()
    if self.state==Fighter.StateInit then

        self:setFighterState(Fighter.StateSpawing)

    elseif self.state==Fighter.StateSpawing then

    elseif self.state==Fighter.StateTargetFinding then


        --find target end
        local target=self:findTarget()

        if target.type==GFighterTypeBig then
            self:set1Flying(target)

        else
            if target:isAtting()==1 then
                if target:isInAttRange()==1 then
                    if target:checkPaired()==0 then
                        target:setTarget(self)
                        target:setFighterState(Fighter.StateTargetAttackingInit)
                        target:setFightPosByAct(self)
                    end
                    self:setFighterState(Fighter.StateTargetAttackingInit)
                    self:setFightPosByAct(target)
                else
                    if target:checkPaired()==1 then
                        self:set1Flying(target)

                    else
                        --                        self:set1Flying(target)
                        --                        self:checkTargetDone()
                        --                        if target.target==self then
                        --                            self:setFighterState(Fighter.StateTargetWaiting)
                        --                        else
                        --
                        --                        end
                        --                        target:setFighterState(Fighter.StateTargetWaiting)
                        self:set2Flying(target)
                        target:setTarget(self)
                    end
                end

            elseif target:isTargetFlying()==1 then

                if target:checkPaired()==1 then
                    local dest=self:getFlyingDest()
                    if dest~=nil then
                        local array=self.gameScene:caculate1PathFromPoint(self,dest)
                        local temp={}
                        temp[#temp+1]=array[2]
                        temp[#temp+1]=array[3]
                        temp[#temp+1]=array[4]
                        self:setFlyingPath(temp)
                        self:setFighterState(Fighter.StateTargetFlying)
                        local p=cc.p(target:getPosition())
                        p=cc.pMidpoint(self:getFlyingDest(),p)
                        local x,y=self.gameScene.map:getTileAt(p.x,p.y)
                        self:setFightPos(cc.p(x,y))
                    end
                else
                    if target.target==self then
                        self:setFighterState(Fighter.StateTargetWaiting)
                    else
                        target:stopFlyingAction()
                        self:set2Flying(target)
                        target:setTarget(self)
                    end

                end

            end
        end

        self:setTarget(target)
    elseif self.state==Fighter.StateTargetWaiting then


    elseif self.state==Fighter.StateTargetFlying then
        self:updateFlying(dt)
        if self:checkTargetDone()==1 then
            self:stopFlyingAction()
            self:setFighterState(Fighter.StateTargetFinding)
        end


    elseif self.state==Fighter.StateTargetAttackingInit then
        self.attX,self.attY=self:getPosition()
        self:setFighterState(Fighter.StateTargetAttacking)
        self:face2Target()
        self:setAttackFlying()
        if self.target.state==Fighter.StateTargetWaiting then
            self.target:setFighterState(Fighter.StateTargetAttackingInit)
        end

    elseif self.state==Fighter.StateTargetAttacking then
        if self:checkTargetDone()==1 then
            self:stopAttackFlyingAction()
            self:setFighterState(Fighter.StateTargetFinding)
        else
            self:updateFire(dt)
        end
    elseif self.state==Fighter.StateDying then

        self:setFighterState(Fighter.StateDead)

    elseif self.state==Fighter.StateDead then

        self:removeSelf()


    end



end

function Fighter:removeSelf()
    self.readyRemove=1
end

function Fighter:onEnter()


end

function Fighter:onExit()


end

function Fighter:onEnterTransitionFinish()


end

function Fighter:onExitTransitionStart()


end

function Fighter:onCleanup()

    print("Fighter onCleanup")
end


return Fighter



