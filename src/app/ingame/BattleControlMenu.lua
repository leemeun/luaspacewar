
local BattleControlMenu = class("BattleControlMenu", function()
    return display.newLayer()
end)

function BattleControlMenu:ctor(parent)
    
    self.gameScene=parent
    self.battleUI=cc.CSLoader:createNode("battleui.csb")
    self:addChild(self.battleUI)
    self.battleUI:setContentSize(cc.size(display.width,display.height))
    ccui.Helper:doLayout(self.battleUI)
    self.selButton=nil
    
    self.deltaTime=0
    self:addUIEvent()
    
end

function BattleControlMenu:getSelButton()

    return self.selButton
end

function BattleControlMenu:getCurData()
    return self.gameScene.data[self.selButton]

end


function BattleControlMenu:addUIEvent()
    local function onclick(sender)
        local select=self.selButton
        if sender:getName()=="but_fighter1" then
            select=1
        end
        if sender:getName()=="but_fighter2" then
            select=2
        end
        if sender:getName()=="but_fighter3" then
            select=3
        end
        if sender:getName()=="but_fighter4" then
            select=4
        end
        if self.selButton~=select then
            self.selButton=select
            self.deltaTime=self.gameScene.data[select]["time"]
            self:makeButtonProgress(select)
        end
        
        
    end
    self.selButton=0

    --    local child = ccui.Helper:seekWidgetByName(node, name)
    local b1=self.battleUI:getChildByName("but_fighter1")
    b1:addClickEventListener(onclick)
    b1=self.battleUI:getChildByName("but_fighter2")
    b1:addClickEventListener(onclick)
    b1=self.battleUI:getChildByName("but_fighter3")
    b1:addClickEventListener(onclick)
    b1=self.battleUI:getChildByName("but_fighter4")
    b1:addClickEventListener(onclick)

end

function BattleControlMenu:progressCallback()
    
    self.gameScene.friendBigFighter:spawFighter(self:getCurData())
end


function BattleControlMenu:stopProgress()
    self:makeButtonProgress(0)
    
end

function BattleControlMenu:makeButtonProgress(index)

    if self.progress~=nil then
        self.progress:removeFromParent()
        self.progress=nil
    end
    if index==nil or index==0 then
        return
    end
    

    local ss="but_fighter"..index
    local b1=self.battleUI:getChildByName(ss)
    local tx,ty=b1:getPosition()
    
    local progress = cc.ProgressTimer:create(display.newSprite("progress.png"))
    progress:setType(cc.PROGRESS_TIMER_TYPE_BAR)
    progress:setMidpoint(cc.p(0, 1))
    progress:setBarChangeRate(cc.p(0, 1))
    
    progress:setPosition(cc.p(tx, ty))
    progress:setPercentage(100)
    
    local to2 = cc.ProgressTo:create(self.deltaTime, 0)
    local function callback()
        progress:setPercentage(100)
        self:progressCallback()
    end
    
    local seq=cc.Sequence:create(to2,cc.CallFunc:create(callback))
    
    progress:runAction(cc.RepeatForever:create(seq))
    self.progress=progress
    self:addChild(progress)
    
end



function BattleControlMenu:onEnter()


end

function BattleControlMenu:onExit()


end

function BattleControlMenu:onEnterTransitionFinish()


end

function BattleControlMenu:onExitTransitionStart()


end

function BattleControlMenu:onCleanup()


end


return BattleControlMenu