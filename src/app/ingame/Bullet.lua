
local Bullet = class("Bullet", function()
    return display.newNode()
end)

Bullet.StateInit=1

Bullet.StateFlyingLow=Bullet.StateInit+1
Bullet.StateFlyingHigh=Bullet.StateFlyingLow+1

Bullet.StateHit=Bullet.StateFlyingHigh+1
Bullet.StateDead=Bullet.StateHit+1
Bullet.StateDone=Bullet.StateDead+1




function Bullet:ctor(scene,owner,target,pros,fire)
    
    self.gameScene=scene
    self.owner=owner
    self.target=target
    self.pros=pros
    self.type=pros.type
    local oxy
    self.firePosition=fire
    if fire==1 then
        oxy=self.owner.csbNode:convertToWorldSpace(cc.p(self.owner.fire1:getPosition()))
    else
        oxy=self.owner.csbNode:convertToWorldSpace(cc.p(self.owner.fire2:getPosition()))
    end
    
    self.orgX=oxy.x
    self.orgY=oxy.y
    self.oldX=self.orgX
    self.oldY=self.orgY
    self.targetX,self.targetY=target:getPosition()
    
    self.type=tonumber(pros.type)
    self.attack=tonumber(pros.attack)
--    self.vel=pros.vel
    self.speed=200
    local res="bullet1.png"
    if self.type==1 then
        res="bullet.png"
        local angle=math.myGetAngle(cc.p(self.targetX-self.orgX,self.targetY-self.orgY))
--        self:setRotation(angle)
        if self.team==GTeamEnemy then
            angle=angle-180
        end
        self:setRotation(90-angle)
        
    end
    
    local sprite=display.newSprite(res)
    sprite:setScale(0.8)
    self.sprite=sprite
    
    self.x=self.orgX
    self.y=self.orgY
    
    self:setPosition(self.x,self.y)
    self:setBulletState(Bullet.StateInit)
    
--    self:initPhysic()
    self:initBullet()
    
    self:addChild(sprite)
    

end

function Bullet:initPhysic()
    
    self:setAnchorPoint(cc.p(0.5, 0.5))
    self:setPhysicsBody(cc.PhysicsBody:createCircle(25*self.sprite:getScale()));
    self:getPhysicsBody():setContactTestBitmask(1);
    --    setCategoryBitmask(4);
    self:getPhysicsBody():setCollisionBitmask(0);
    
    
    local function onContactBegin()
        self:setBulletState(Bullet.StateDead)
    end
    
    
    local function onContactSeperate()
        self:setBulletState(Bullet.StateFlyingHigh)
    end
    
    local eventDispatcher = self:getEventDispatcher()
    
    local contactListener = cc.EventListenerPhysicsContactWithBodies:create(self:getPhysicsBody(), self.owner:getPhysicsBody());
    contactListener:registerScriptHandler(onContactSeperate, cc.Handler.EVENT_PHYSICS_CONTACT_SEPERATE);
    eventDispatcher:addEventListenerWithSceneGraphPriority(contactListener, self.sprite);
    
    local contactListener = cc.EventListenerPhysicsContactWithBodies:create(self:getPhysicsBody(), self.target:getPhysicsBody());
    contactListener:registerScriptHandler(onContactBegin, cc.Handler.EVENT_PHYSICS_CONTACT_BEGIN);
    eventDispatcher:addEventListenerWithSceneGraphPriority(contactListener, self.sprite);
    
    
end


function Bullet:initBullet()
    
end

function Bullet:setBulletState(sta)

    if sta==Bullet.StateFlyingLow then
        
    elseif self.state==Bullet.StateFlyingHigh then
        
        self.gameScene:changeBullet2High(self)
    end

    self.state=sta

end


function Bullet:checkBulletDone()

    if self.x<-BulletCheckWidth or self.x>BulletCheckWidth+CONFIG_SCREEN_WIDTH then
        return true
    end
    
    if self.y<-BulletCheckHeight or self.y>BulletCheckHeight+CONFIG_SCREEN_HEIGHT then
        return true
    end

    return false
end


function Bullet:updateFlying(dt)
    self.x,self.y=self:getPosition()
    if self.x==self.oldX and self.y==self.oldY then

    else
        local angle=math.myGetAngle(cc.p(self.x-self.oldX,self.y-self.oldY))
--        self:setRotation(angle)
        if self.team==GTeamEnemy then
            angle=angle-180
        end
        self:setRotation(90-angle)

    end
    self.oldX=self.x
    self.oldY=self.y

end

function Bullet:moveEndCallback()
    
    self:setBulletState(Bullet.StateHit)
    if self.target~=nil and self.target.isLive~=nil and self.target:isLive()==1 then
        self.target:hurt(self)
    end
    
    
end

function Bullet:setBulletPath(dt)

    local function callback()
        self:moveEndCallback()
    end

    if self.type==1 then
        local action=cc.MoveTo:create(0.1,cc.p(self.targetX,self.targetY))
        --    action=cc.MoveTo:create(0.5,tv2)
        
        local se=cc.Sequence:create(action,cc.CallFunc:create(callback))
        
        self.moveAction=se
        self:runAction(self.moveAction)
    elseif self.type==2 then
        local tv2=cc.p(self.targetX,self.targetY)
        local ov2=cc.p(self.orgX,self.orgY)
        local sub=cc.pSub(tv2,ov2)
        local v1=cc.vec3(sub.x,sub.y,0)
        local v2=cc.vec3(0,0,1)

        local dst=cc.vec3(0,0,0)
        dst=vec3_cross(v1,v2,dst)
        dst=cc.vec3normalize(dst)
        local wid=40
        if self.owner.team==GTeamEnemy then
            if self.firePosition==2 then
                wid=-40
            end
        else
            if self.firePosition==1 then
                wid=-40
            end
        end

        local off=cc.p(dst.x*wid,dst.y*wid)
        local pp=cc.pAdd(cc.pMidpoint(tv2,ov2),off)

        local array = {
            cc.p(0,0),
            cc.pSub(pp,ov2),
            cc.pSub(tv2,ov2),
        }
        local action = cc.CardinalSplineBy:create(0.5, array, 0)
        --    action=cc.MoveTo:create(0.5,tv2)
        local se=cc.Sequence:create(action,cc.CallFunc:create(callback))
        self.moveAction=se
        self:runAction(self.moveAction)
    else
    
    end
    
    
end

function Bullet:updateBullet(dt)
    
    if self.state==Bullet.StateInit then
        
        self:setBulletPath(dt)
        self:setBulletState(Bullet.StateFlyingLow)
        
    elseif self.state==Bullet.StateFlyingLow then
        self:updateFlying(dt)
        
        
        
    elseif self.state==Bullet.StateFlyingHigh then
        self:updateFlying(dt)
  
    elseif self.state==Bullet.StateHit then
        self:setBulletState(Bullet.StateDead)
    
    elseif self.state==Bullet.StateDead then
        self:handBulletOver()
        
    end
    
    
end


function Bullet:handBulletOver()

    self:setBulletState(Bullet.StateDone)
    self.gameScene:removeBullet(self)
    

end


function Bullet:onEnter()


end

function Bullet:onExit()


end

function Bullet:onEnterTransitionFinish()


end

function Bullet:onExitTransitionStart()


end

function Bullet:onCleanup()


end


return Bullet