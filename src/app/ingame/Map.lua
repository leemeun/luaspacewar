

local Map = class("Map",function(parent,property)
    local layer=display.newLayer()
    return layer
end)


Map.rows=11
Map.cols=9
Map.tileWidth=105
Map.tileHeight=92
Map.MaxPath=20
Map.MaxGridNum=59

Map.TileStateNormal=1
Map.TileStateMove=Map.TileStateNormal+1
Map.TileStateAttack=Map.TileStateMove+1

Map.TileFlagNothing=0
Map.TileFlagBar=Map.TileFlagNothing+1
Map.ImageNormal="square.png"
Map.ImageMove="square_s.png"
Map.ImageAttack="grid_light_red.png"
Map.ImageBar="grid_durk_red.png"
-- Map.ImageMapBg = "testMap.png"
Map.ImageMapBg = "mytest_bg0.png"
Map.ImageGridBg = "showgrid_bg.png"
Map.ImageButBg = "but_show_grid.png"

Map.FriPos=cc.p(10,4)
Map.EnePos=cc.p(0,4)

function Map:testTileFlag()
--  self.tilesFlag[3][4]=1
--  self.tiles[3][4]:setColor(ccc3(0, 0, 255))
end

function Map:ctor(parent,property)
    self.parent=parent
    self.tilesProperty=property
    self.tilesFlag=create2DArray(Map.rows,Map.cols)
    self.path = {}
    self.tiles=create2DArray(Map.rows,Map.cols)
    self.moves=create2DArray(Map.rows,Map.cols)
    self.testFlag=create2DArray(Map.rows,Map.cols)
    self.curTiles=create2DArray(Map.rows,Map.cols)
    self.tapedTile=nil
    self.tileTaped=false
    self.showGridEnable = false
    
    self.starX = 10 + Map.tileWidth * 0.5
    self.starY = display.height - Map.tileHeight * 0.5 -170
    local width = Map.tileWidth * 0.75
    local xOff=0
    local yOff=0


    -- menuLayer = ui.newMenu({})
    for i=0,Map.cols-1 do
        xOff = self.starX + width * (i)
        local cl=0
        if i%2==0 then
            cl=Map.rows-1
        else
            cl=Map.rows-2
        end
        for j=0,cl do
            if i%2==0 then
                yOff =  self.starY - Map.tileHeight * (j)
            else
                yOff =  self.starY - Map.tileHeight * 0.5 - Map.tileHeight * (j)
            end

            local item = display.newSprite(Map.ImageNormal, xOff, yOff)
            -- local text1=ui.newTTFLabel({
            --  x = xOff,
            --  y = yOff,
            --  text=i..","..j,
            -- }
            -- )
            
            local  label = cc.Label:createWithSystemFont(j..","..i,"Arial", 28)
            label:setAnchorPoint(cc.p(0.5, 0.5))
            label:setPosition(cc.p(xOff,yOff))
            self:addChild(label)
            
            item.row=j
            item.col=i
            item.state=Map.TileStateNormal
            self.tiles[j][i]=item
            self:addChild(item)
            item:setScale(0.9)
            
            --            item:scale(0.9)
            -- self:addChild(text1)
        end
    end
    -- self:addChild(menuLayer)
    -- self:addTouchEventListener(handler(self, self.onTouch))
    self:enableNodeEvents()

    self:testTileFlag()


    local function onTouchesBegan(touches, event)
        local beginPos = touches[1]:getLocation()
        local tx,ty=self:getTileAt(beginPos.x,beginPos.y)
        print("onTouchesBegan:",tx,ty)

    end


    local listener = cc.EventListenerTouchAllAtOnce:create()
    listener:registerScriptHandler(onTouchesBegan,cc.Handler.EVENT_TOUCHES_BEGAN )
    --    listener:registerScriptHandler(onTouchesMoved,cc.Handler.EVENT_TOUCH_MOVED )

    local eventDispatcher = self:getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(listener, self)


end

function Map:onTouch(event, x, y)
    if event == "began" then
        return self:onTouchBegan(x, y)
    elseif event == "moved" then
        self:onTouchMoved(x, y)
    elseif event == "ended" then
        self:onTouchEnded(x, y)
    else -- cancelled
        self:onTouchCancelled(x, y)
    end
end

function Map:onTouchBegan(x, y)
    local tx,ty=self:getTileAt(x,y)
    self.tapedTile={tx,ty}
    self.tileTaped=false


    return true
end


function Map:onTouchMoved(x, y)

    return true
end

function Map:onTouchEnded(x, y)
    local tx,ty=self:getTileAt(x,y)
    if self.tapedTile[1]==tx and self.tapedTile[2]==ty then
        self.tileTaped=true
    end
    return true
end

function Map:resetTouchEvent()
    if self.tileTaped==true then
        self.tileTaped=false
        self.tapedTile=nil
    end
end

function Map:setTileImage(x,y,name)
    local sp = display.newSprite(name)
    self.tiles[x][y]:setDisplayFrame(CCSpriteFrame:createWithTexture(sp:getTexture(),sp:getTextureRect()))
    local flip = CCOrbitCamera:create(0.15, 1, 1, 90, 90, 0, 45)
    self.tiles[x][y]:runAction(flip)
end

function Map:resetTileStatus()
    local i=0
    local j=0
    self.moves=create2DArray(Map.rows,Map.cols)
    for i=0, Map.rows do
        for j=0, Map.cols do
            if self:checkTileBound(i,j) then
                if self.tilesFlag[i][j]==Map.TileFlagNothing then
                    self:setTileImage(i,j,self.ImageNormal)
                    --                  self.tiles[i][j]:setColor(ccc3(255, 255, 255))
                end
                if self.tilesFlag[i][j]==Map.TileFlagBar then
                    self:setTileImage(i,j,self.ImageBar)
                    --                  self.tiles[i][j]:setColor(ccc3(0, 0, 255))
                end
            end
        end
    end

end

function Map:checkTileMovable(tx,ty)
    if self:checkTileBound(tx,ty)==false then
        return false
    end

    if self.tilesFlag[tx][ty]==Map.TileFlagNothing then
        for i=1,#self.parent.enemies do
            if self.parent.curActor~=self.parent.enemies[i] then
                if self.parent.enemies[i]:isLive() and self.parent.enemies[i].tileX==tx and self.parent.enemies[i].tileY==ty then
                    return false
                end
            end
        end
        for i=1,#self.parent.friends do
            if self.parent.curActor~=self.parent.friends[i] then
                if self.parent.friends[i]:isLive() and  self.parent.friends[i].tileX==tx and self.parent.friends[i].tileY==ty then
                    return false
                end
            end
        end
        return true
    end
    return false

end


function Map:getMoveRange(x,y,steps)
    self.moves=create2DArray(Map.rows,Map.cols)
    self.moves[x][y]=1
    local arr={{x,y}}
    for i=1,steps do
        for a=1,#arr do
            local ta=self:getSuroundTiles(arr[a][1],arr[a][2])
            for b=1,#ta do
                if self:checkTileMovable(ta[b][1],ta[b][2]) then
                    self.moves[ta[b][1]][ta[b][2]]=1
                end
            end
        end
        arr={}
        for m=0,Map.rows-1 do
            for n=0,Map.cols-1 do
                if self.moves[m][n]==1 then
                    arr[#arr+1]={m,n}
                end
            end
        end
    end
    return arr
end

function Map:get2Ranges(x,y,move,attack)
    self.moves=create2DArray(Map.rows,Map.cols)
    self.moves[x][y]=1
    local arr={{x,y}}
    for i=1,move do
        for a=1,#arr do
            local ta=self:getSuroundTiles(arr[a][1],arr[a][2])
            for b=1,#ta do
                if self:checkTileMovable(ta[b][1],ta[b][2]) then
                    self.moves[ta[b][1]][ta[b][2]]=1
                end
            end
        end
        arr={}
        for m=0,Map.rows-1 do
            for n=0,Map.cols-1 do
                if self.moves[m][n]==1 then
                    arr[#arr+1]={m,n}
                end
            end
        end
    end
    local arr1={}
    for tt=1,#arr do
        arr1[tt]=arr[tt]
    end
    for i=1,attack do
        for a=1,#arr1 do
            local ta=self:getSuroundTiles(arr1[a][1],arr1[a][2])
            for b=1,#ta do
                if self.moves[ta[b][1]][ta[b][2]]~=1 then
                    self.moves[ta[b][1]][ta[b][2]]=2
                end
            end
        end
        arr1={}
        for m=0,Map.rows-1 do
            for n=0,Map.cols-1 do
                if self.moves[m][n]==2 then
                    arr1[#arr1+1]={m,n}
                end
            end
        end
    end

    return arr,arr1

end

function Map:getMARange(x,y,move,attack)
    self.mas=create2DArray(Map.rows,Map.cols)
    self.mas[x][y]=1
    local arr={{x,y}}
    for i=1,move do
        for a=1,#arr do
            local ta=self:getSuroundTiles(arr[a][1],arr[a][2])
            for b=1,#ta do
                if self:checkTileMovable(ta[b][1],ta[b][2]) then
                    self.mas[ta[b][1]][ta[b][2]]=1
                end
            end
        end
        arr={}
        for m=0,Map.rows-1 do
            for n=0,Map.cols-1 do
                if self.mas[m][n]==1 then
                    arr[#arr+1]={m,n}
                end
            end
        end
    end

    for i=1,attack do
        for a=1,#arr do
            local ta=self:getSuroundTiles(arr[a][1],arr[a][2])
            for b=1,#ta do
                self.mas[ta[b][1]][ta[b][2]]=1
            end
        end
        arr={}
        for m=0,Map.rows-1 do
            for n=0,Map.cols-1 do
                if self.mas[m][n]==1 then
                    arr[#arr+1]={m,n}
                end
            end
        end
    end
    return arr
end

function Map:getAttackRange(x,y,steps)
    self.attacks=create2DArray(Map.rows,Map.cols)
    self.attacks[x][y]=1
    local arr={{x,y}}
    for i=1,steps do
        for a=1,#arr do
            local ta=self:getSuroundTiles(arr[a][1],arr[a][2])
            for b=1,#ta do
                self.attacks[ta[b][1]][ta[b][2]]=1
            end
        end
        arr={}
        for m=0,Map.rows-1 do
            for n=0,Map.cols-1 do
                if self.attacks[m][n]==1 then
                    arr[#arr+1]={m,n}
                end
            end
        end
    end
    return arr

end

function Map:getAttackTile()
    if self.tileTaped==true then
        local x=self.tapedTile[1]
        local y=self.tapedTile[2]
        if x==-1 or y==-1 then
            return {-1,-1}
        else
            if self.attacks[x][y]==1 then
                return {x,y}
            else
                return {-1,-1}
            end
        end

    end
    return nil


end

function Map:getMoveTile()

    if self.tileTaped==true then
        local x=self.tapedTile[1]
        local y=self.tapedTile[2]
        if x==-1 or y==-1 then
            return {-1,-1}
        else
            if self.moves[x][y]==1 then
                return {x,y}
            else
                return {-1,-1}
            end
        end

    end
    return nil
end

function Map:getMoveAndAttackTile()
    if self.tileTaped==true then
        local x=self.tapedTile[1]
        local y=self.tapedTile[2]
        if x~=-1 then
            if self.moves[x][y]==1 then
                return 1,{x,y}
            elseif  self.moves[x][y]==2 then
                return 2,{x,y}
            else
                return 1,{-1,-1}
            end
        else
            return 1,{-1,-1}
        end

    end
    return nil


end

function Map:updateCurTiles()

    for i=0,Map.cols-1 do
        local m=0
        if i%2==0 then
            m=Map.rows-1
        else
            m=Map.rows-2
        end
        for j=0,Map.rows-1 do
            self.curTiles[j][i]=1
        end
        
    end
    local temp=self.parent.friends
    local tx,ty
    local arr
    for i=1,#temp do
        
        tx=temp[i].fightPos.x
        ty=temp[i].fightPos.y
        self.curTiles[tx][ty]=2
        arr=self:getSuroundTiles(tx,ty)
        for i=1,#arr do
            self.curTiles[arr[i][1]][arr[i][2]]=2
        end
    end
    
    temp=self.parent.enemies
    for i=1,#temp do
        tx=temp[i].fightPos.x
        ty=temp[i].fightPos.y
        self.curTiles[tx][ty]=2
        arr=self:getSuroundTiles(tx,ty)
        for i=1,#arr do
            self.curTiles[arr[i][1]][arr[i][2]]=2
        end
        
    end
    
    
    local sp1 = display.newSprite(Map.ImageNormal)
    local sp2 = display.newSprite(Map.ImageMove)
    
    for i=0,Map.cols-1 do
        for j=0,Map.rows-1 do
            local frame=nil
            if self.curTiles[j][i]==2 then
                frame=display.newSpriteFrame(sp2:getTexture(),sp2:getTextureRect())
            else  
                frame=display.newSpriteFrame(sp1:getTexture(),sp1:getTextureRect())
            end
            if self.tiles[j][i]~=0 then
                self.tiles[j][i]:setSpriteFrame(frame)
            end
            
        end
    end
    
end

function Map:checkTileEdge(tx,ty)
    if ty==0 or tx==0 then
        return 1
    end
    if ty==Map.cols-1 then
        return 1
    end
    
    if ty%2==0 then
        if tx==Map.cols-1 then
            return 1
        end
    else
        if tx==Map.cols-2 then
            return 1
        end
    end
    
    return 0
end

function Map:checkTileValid(tx,ty)
    local flag=self:checkTileEdge(tx,ty)
    if flag==0 then
        if tx~=9 and ty~=4 then
            return 0 
        end
        if tx~=1 and ty~=4 then
            return 0 
        end
    else
        return 0     
    end

    return 1
end


function Map:findFightingPoint(act1,act2)
    
    
    local t1x,t1y=act1:getPosition()
    local t2x,t2y=act2:getPosition()
    local midx=(t1x+t2x)/2
    local midy=(t1y+t2y)/2
    local tx,ty=self:getTileAt(midx,midy)
    
    if tx==-1 then
        local x0,y0=self.tiles[Map.FriPos.x][Map.FriPos.y]:getPosition()
        local x1,y1=self.tiles[Map.EnePos.x][Map.EnePos.y]:getPosition()
        if getDistance(midx,midy,x0,y0)>getDistance(midx,midy,x1,y1) then
            tx,ty=Map.EnePos.x,Map.EnePos.y
        else
            tx,ty=Map.FriPos.x,Map.FriPos.y
        end
    end
    
    if self.curTiles[tx][ty]==1 then
        return self.tiles[tx][ty]:getPosition()
    end
    
    local visit=create2DArray(Map.rows,Map.cols)
    local parent=create2DArray(Map.rows,Map.cols)
    visit[tx][ty]=1
    local arr
    local arr1
    local flag=0
    local path={{tx,ty}}
    for a=1,90 do
        tx=path[a][1]
        ty=path[a][2]
        arr=self:sortFindingTile(t1x,t1y,t2x,t2y,self:getSuroundTiles(tx,ty))
        for i=1,#arr do
            flag=0
            arr1=self:sortFindingTile(t1x,t1y,t2x,t2y,self:getSuroundTiles(arr[i][1],arr[i][2]))
            for m=1,#arr1 do
                if self.curTiles[arr1[m][1]][arr1[m][2]]==2 then
                    flag=1
                    break;
                end
            end
            if flag==0 then
                return self.tiles[arr[i][1]][arr[i][2]]:getPosition()
            end
            if visit[arr[i][1]][arr[i][2]]==0 then
                path[#path+1]=arr[i]
                visit[arr[i][1]][arr[i][2]]=1
            end
--            if flag==0 then
--                local arr2=self:getSuroundTiles(arr[i][1],arr[i][2])
--                self.curTiles[arr[i][1]][arr[i][2]]=2
--                for i=1,#arr2 do
--                    self.curTiles[arr2[i][1]][arr2[i][2]]=2
--                end
--                return self.tiles[arr[i][1]][arr[i][2]]:getPosition()
--            end
--            if visit[arr[i][1]][arr[i][2]]==0 then
--                path[#path+1]=arr[i]
--                visit[arr[i][1]][arr[i][2]]=1
--            end
        end
    end
    
    return -1,-1
end

function Map:sortFindingTile(x1,y1,x2,y2,arr)
    
    local temp={}
    local dis1=0
    local x3,y3
    local dis2=0
    local flag=0
    for i=1,#arr do
        if #temp==0 then
            temp[1]=arr[i]
        else
            flag=0
            x3,y3=self.tiles[arr[i][1]][arr[i][2]]:getPosition()
            dis1=math.abs(getDistance(x1,y1,x3,y3)-getDistance(x2,y2,x3,y3))
            for a=1,#temp do
                x2,y2=self.tiles[temp[a][1]][temp[a][2]]:getPosition()
                dis2=math.abs(getDistance(x1,y1,x3,y3)-getDistance(x2,y2,x3,y3))
                if dis1>dis2 then
                    table.insert(temp,a,arr[i])
                    flag=1
                end
            end
            if flag==0 then
                temp[#temp+1]=arr[i]
            end
        end
    end
    return temp
end

--屏幕坐标换算成tile坐标
function Map:getTileAt(x,y)
    local w=Map.tileWidth*0.75
    local h=Map.tileHeight
    local diffX=x-(self.starX-Map.tileWidth/2)
    local diffY=self.starY+Map.tileHeight/2-y
    local tempR=math.floor(diffY/h)
    local tempC=math.floor(diffX/w)
    -- print(diffX,diffY,tempR,tempC)
    local t00=nil
    --    tempR-1>=0 and tempC-1>=0 and tempR-1<Map.rows and tempC-1<Map.cols
    if self:checkTileBound(tempR-1,tempC-1) then
        t00=self.tiles[tempR-1][tempC-1]
    end
    local t01=nil
    --    tempR-1>=0 and tempC>=0 and tempR-1<Map.rows and tempC<Map.cols
    if self:checkTileBound(tempR-1,tempC) then
        t01=self.tiles[tempR-1][tempC]
    end
    local t10=nil
    --    tempR>=0 and tempC-1>=0 and tempR<Map.rows and tempC-1<Map.cols
    if self:checkTileBound(tempR,tempC-1) then
        t10=self.tiles[tempR][tempC-1]
    end
    local t11=nil
    --    tempR>=0 and tempC>=0 and tempR<Map.rows and tempC<Map.cols
    if self:checkTileBound(tempR,tempC) then
        t11=self.tiles[tempR][tempC]
    end

    local temp=-1
    local dis=9999999
    if t00~=nil then
        local centerX=t00:getPositionX()
        local centerY=t00:getPositionY()
        local td=(centerX-x)^2+(centerY-y)^2
        -- print("t00",tempR-1,tempC-1,centerX,centerY,td)
        if dis>td then
            dis=td
            temp=0
        end

    end
    if t01~=nil then
        local centerX=t01:getPositionX()
        local centerY=t01:getPositionY()
        local td=(centerX-x)^2+(centerY-y)^2
        -- print("t01",tempR-1,tempC,centerX,centerY,td)
        if dis>td then
            dis=td
            temp=1
        end

    end
    if t10~=nil then
        local centerX=t10:getPositionX()
        local centerY=t10:getPositionY()
        local td=(centerX-x)^2+(centerY-y)^2
        -- print("t10",tempR,tempC-1,centerX,centerY,td)
        if dis>td then
            dis=td
            temp=2
        end

    end
    if t11~=nil then
        local centerX=t11:getPositionX()
        local centerY=t11:getPositionY()
        local td=(centerX-x)^2+(centerY-y)^2
        -- print("t11",tempR,tempC,centerX,centerY,td)
        if dis>td then
            dis=td
            temp=3
        end

    end
    local rx=-1
    local ry=-1
    if temp==0 then
        rx=tempR-1
        ry=tempC-1
    end
    if temp==1 then
        rx=tempR-1
        ry=tempC
    end
    if temp==2 then
        rx=tempR
        ry=tempC-1
    end
    if temp==3 then
        rx=tempR
        ry=tempC
    end
    if rx~=-1 then
        local d=getDistance(x,y,self.tiles[rx][ry]:getPositionX(),self.tiles[rx][ry]:getPositionY())
        if d>Map.tileHeight/2+10 then
            rx=-1
            ry=-1
        end
    end


    return rx,ry
        -- if tempR%2==0 then
        --  tempC=toint((diffX-Map.tileWidth * 0.5)/w)+1
        -- end
end


function Map:getSuroundTiles(i,j)
    local array={}
    local compare=Map.tileHeight^2+10
    if self:checkTileBound(i-1,j-1) then
        local temp=(self.tiles[i][j]:getPositionX()-self.tiles[i-1][j-1]:getPositionX())^2
        temp=temp+(self.tiles[i][j]:getPositionY()-self.tiles[i-1][j-1]:getPositionY())^2
        if temp<compare then
            array[#array+1]={i-1,j-1}
        end
    end
    if self:checkTileBound(i-1,j) then
        local temp=(self.tiles[i][j]:getPositionX()-self.tiles[i-1][j]:getPositionX())^2
        temp=temp+(self.tiles[i][j]:getPositionY()-self.tiles[i-1][j]:getPositionY())^2
        if temp<compare then
            array[#array+1]={i-1,j}
        end
    end
    if self:checkTileBound(i-1,j+1) then
        local temp=(self.tiles[i][j]:getPositionX()-self.tiles[i-1][j+1]:getPositionX())^2
        temp=temp+(self.tiles[i][j]:getPositionY()-self.tiles[i-1][j+1]:getPositionY())^2
        if temp<compare then
            array[#array+1]={i-1,j+1}
        end
    end
    if self:checkTileBound(i,j-1) then
        local temp=(self.tiles[i][j]:getPositionX()-self.tiles[i][j-1]:getPositionX())^2
        temp=temp+(self.tiles[i][j]:getPositionY()-self.tiles[i][j-1]:getPositionY())^2
        if temp<compare then
            array[#array+1]={i,j-1}
        end
    end
    if self:checkTileBound(i,j+1) then
        local temp=(self.tiles[i][j]:getPositionX()-self.tiles[i][j+1]:getPositionX())^2
        temp=temp+(self.tiles[i][j]:getPositionY()-self.tiles[i][j+1]:getPositionY())^2
        if temp<compare then
            array[#array+1]={i,j+1}
        end
    end
    if self:checkTileBound(i+1,j-1) then
        local temp=(self.tiles[i][j]:getPositionX()-self.tiles[i+1][j-1]:getPositionX())^2
        temp=temp+(self.tiles[i][j]:getPositionY()-self.tiles[i+1][j-1]:getPositionY())^2
        if temp<compare then
            array[#array+1]={i+1,j-1}
        end
    end
    if self:checkTileBound(i+1,j) then
        local temp=(self.tiles[i][j]:getPositionX()-self.tiles[i+1][j]:getPositionX())^2
        temp=temp+(self.tiles[i][j]:getPositionY()-self.tiles[i+1][j]:getPositionY())^2
        if temp<compare then
            array[#array+1]={i+1,j}
        end
    end
    if self:checkTileBound(i+1,j+1) then
        local temp=(self.tiles[i][j]:getPositionX()-self.tiles[i+1][j+1]:getPositionX())^2
        temp=temp+(self.tiles[i][j]:getPositionY()-self.tiles[i+1][j+1]:getPositionY())^2
        if temp<compare then
            array[#array+1]={i+1,j+1}
        end
    end

    return array
end

function Map:checkNear(x1,y1,x2,y2)
    local arr=self:getSuroundTiles(x1,y1)
    for i=1,#arr do
        if x2==arr[i][1] and y2==arr[i][2] then
            return true
        end
    end
    return false
end

function Map:getMutualNear(x1,y1,x2,y2)
    local arr1=self:getSuroundTiles(x1,y1)
    local arr2=self:getSuroundTiles(x2,y2)
    local arr={}
    for a=1,#arr1 do
        for b=1,#arr2 do
            if arr1[a][1]==arr2[b][1] and arr1[a][2]==arr2[b][2] then
                arr[#arr+1]={arr1[a][1],arr2[a][2]}
            end
        end
    end
    return arr
end

function Map:getBackTile(x1,y1,x2,y2)
    local index=-1
    local dx=self.tiles[x2][y2]:getPositionX()-self.tiles[x1][y1]:getPositionX()
    local dy=self.tiles[x2][y2]:getPositionY()-self.tiles[x1][y1]:getPositionY()
    local tx=self.tiles[x2][y2]:getPositionX()+dx
    local ty=self.tiles[x2][y2]:getPositionY()+dy
    return self:getTileAt(tx,ty)
end

function Map:checkTileBound(i,j)
    if j>=0 and j<Map.cols then
        if j%2==1 then
            if i>=0 and i<Map.rows-1 then
                return true
            end
        else
            if i>=0 and i<Map.rows then
                return true
            end
        end
    end
    return false
end

function Map:checkTileAttackable(tx,ty)

    return true

end


function Map:APath(x,y,dx,dy)
    local path={}
    local visit=create2DArray(Map.rows,Map.cols)
    local parent=create2DArray(Map.rows,Map.cols)
    visit[x][y]=1
    parent[x][y]={-1,-1}
    local arr={{x,y}}
    local loaded={{x,y}}
    for i=1,Map.MaxPath do
        for a=1,#loaded do
            -- local ta=self:getSuroundTiles(arr[a][1],arr[a][2])
            local ta=self:getSuroundTiles(loaded[a][1],loaded[a][2])
            ta=self:prioritySort(ta,dx,dy)
            for b=1,#ta do
                if visit[ta[b][1]][ta[b][2]]==0 and self:checkTileAttackable(ta[b][1],ta[b][2]) then
                    local node={ta[b][1],ta[b][2]}
                    loaded[#loaded+1]=node
                    -- parent[ta[b][1]][ta[b][2]]={arr[a][1],arr[a][2]}
                    parent[ta[b][1]][ta[b][2]]={loaded[a][1],loaded[a][2]}
                    -- print("tile:",ta[b][1],ta[b][2],parent[ta[b][1]][ta[b][2]][1],parent[ta[b][1]][ta[b][2]][2])
                    if ta[b][1]==dx and ta[b][2]==dy then
                        path[#path+1]={ta[b][1],ta[b][2]}
                        for mm=1,Map.MaxPath do
                            local size=#path
                            -- print("path:",path[size][1],path[size][2])
                            if parent[path[size][1]][path[size][2]][1]~=-1 then
                                path[#path+1]={parent[path[size][1]][path[size][2]][1],parent[path[size][1]][path[size][2]][2]}
                            else
                                local result={}
                                for kk=1,#path do
                                    result[kk]=path[#path+1-kk]
                                end
                                return result
                            end

                        end
                    end
                    visit[ta[b][1]][ta[b][2]]=1
                end

            end
        end
        -- arr={}
        -- for m=0,Map.rows do
        --     for n=0,Map.cols do
        --         if visit[m][n]==1 then
        --             arr[#arr+1]={m,n}
        --         end
        --     end
        -- end
    end
    return path
end



function Map:AStarPath(x,y,dx,dy,flag)
    local path={}
    local visit=create2DArray(Map.rows,Map.cols)
    local parent=create2DArray(Map.rows,Map.cols)
    visit[x][y]=1
    parent[x][y]={-1,-1}
    local arr={{x,y}}
    local loaded={{x,y}}
    for i=1,Map.MaxPath do
        for a=1,#loaded do
            -- local ta=self:getSuroundTiles(arr[a][1],arr[a][2])
            local ta=self:getSuroundTiles(loaded[a][1],loaded[a][2])

            ta=self:prioritySort(ta,dx,dy)
            for b=1,#ta do
                local test=false
                if flag==1 then
                    test=ta[b][1]==dx and ta[b][2]==dy
                end

                if visit[ta[b][1]][ta[b][2]]==0 and ( (test) or self:checkTileMovable(ta[b][1],ta[b][2])) then
                    local node={ta[b][1],ta[b][2]}
                    loaded[#loaded+1]=node
                    -- parent[ta[b][1]][ta[b][2]]={arr[a][1],arr[a][2]}
                    parent[ta[b][1]][ta[b][2]]={loaded[a][1],loaded[a][2]}
                    -- print("tile:",ta[b][1],ta[b][2],parent[ta[b][1]][ta[b][2]][1],parent[ta[b][1]][ta[b][2]][2])
                    if ta[b][1]==dx and ta[b][2]==dy then
                        path[#path+1]={ta[b][1],ta[b][2]}
                        for mm=1,Map.MaxPath do
                            local size=#path
                            if parent[path[size][1]][path[size][2]][1]~=-1 then
                                path[#path+1]={parent[path[size][1]][path[size][2]][1],parent[path[size][1]][path[size][2]][2]}
                            else
                                local result={}
                                for kk=1,#path do
                                    result[kk]=path[#path+1-kk]
                                end
                                return result
                            end

                        end
                    end
                    visit[ta[b][1]][ta[b][2]]=1
                end

            end
        end
        -- arr={}
        -- for m=0,Map.rows do
        --     for n=0,Map.cols do
        --         if visit[m][n]==1 then
        --             arr[#arr+1]={m,n}
        --         end
        --     end
        -- end
    end
    return path
end

function Map:prioritySort(arr,dx,dy)
    local temp={}
    local td={}
    for i=1,#arr do
        local dd=(self.tiles[arr[i][1]][arr[i][2]]:getPositionX()-self.tiles[dx][dy]:getPositionX())^2
        dd=dd+(self.tiles[arr[i][1]][arr[i][2]]:getPositionY()-self.tiles[dx][dy]:getPositionY())^2
        td[i]=dd
        temp[i]=i
    end
    for i=1,#arr do
        local dd=td[i]
        for j=i,#arr do
            if dd>td[j] then
                local dd1=td[i]
                td[i]=td[j]
                td[j]=dd1
                dd=td[i]
                local index=temp[i]
                temp[i]=temp[j]
                temp[j]=index
            end
        end
    end
    local tt={}
    for i=1,#arr do
        tt[i]=arr[temp[i]]
    end
    return tt
end

function Map:onItemTap(item)

    print(item.row,item.col)
    if self.state==Map.TileStateNormal then

    elseif self.state==Map.TileStateMove then

    elseif self.state==Map.TileStateAttack then

    end

end

function Map:onEnter()

end

function Map:onExit()


end

function Map:Convert2XY(index)
    local array={0,9,17,26,34,43,51,60}
    for i=1,#array do
        if index<array[i] then
            return i-2,index-array[i-1]
        end
    end
    return nil
end

function Map:getEnemiesFromData(mapdata)
    local array={}
    for i=0,Map.MaxGridNum do
        local temp=mapdata["map_"..i]
        if temp~="0" and temp~="" then
            local x,y=self:Convert2XY(i)
            local rs={x,y}
            local t=split(temp,",")
            for a=1,#t do
                rs[#rs+1]=t[a]
            end
            array[#array+1]=rs
        end
    end
    return array
end

--显示格子底图：
function Map:showAllGrids()

    gridBg:setVisible(self.showGridEnable)
    self.showGridEnable = not self.showGridEnable
end


return Map
