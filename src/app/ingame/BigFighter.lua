
local Fighter=require("app.ingame.Fighter")

local BigFighter = class("BigFighter", function()
    return display.newNode()
end)


BigFighter.StateInit=1
BigFighter.StateFireInit=BigFighter.StateInit+1
BigFighter.StateNormalFlying=BigFighter.StateFireInit+1

BigFighter.StateTargetDying=BigFighter.StateNormalFlying+1
BigFighter.StateTargetDead=BigFighter.StateTargetDying+1

function BigFighter:ctor(parent,x,y,pros)
    GID=GID+1
    self.gID=GID
    self.orgX=x
    self.orgY=y
    self.x=x
    self.y=y
    self.pros=pros
    self.gameScene=parent
    self.team=self.pros.team
    self.moveSpeed=100
    self.moveRange=50
    self.type=GFighterTypeBig

    local sprite=display.newSprite("fighter2.png")
    sprite:setScale(0.8)
    if self.pros.team==GTeamEnemy then
        sprite:setFlippedY(true)
        self:setAutoSpawning()
    end
    self:addChild(sprite)
    self:initFighter()
    self:setPosition(self.x,self.y)
    
    self.selectFighter=1

end


function BigFighter:isLive()

    return 1
end

function BigFighter:isDead()

    return 0
end

function BigFighter:initFighter()
    self.state=BigFighter.StateInit
    self.deltaTime=12
    self.curTime=0
    self.lastSpawtime=-20
end



function BigFighter:fightTest()

    local ff=self.gameScene.enemyBigFighter
    local fight=Fighter.new(self.gameScene,ff.x,ff.y,ff.pros,ff)
    self.gameScene:add2Team(fight)

    ff=self.gameScene.friendBigFighter
    local fight1=Fighter.new(self.gameScene,ff.x,ff.y,ff.pros,ff)
    self.gameScene:add2Team(fight1)

    fight.target=fight1
    fight1.target=fight

    local array=self.gameScene:caculate2Path(fight,fight1)
    local temp={}
    temp[#temp+1]=array[2]
    temp[#temp+1]=array[3]
    temp[#temp+1]=array[4]
    fight:setFlyingPath(temp)
    local action=cc.BezierTo:create(1,temp)
    local function callback1()
        --        fight:removeSelf()
        --        fight:setFighterState(Fighter.StateTargetAttackingInit)
        print("xxxxx")
    end

    fight:runAction(cc.Sequence:create(action,cc.CallFunc:create(callback1)))
    fight:setFighterState(Fighter.StateTargetFlying)

    temp={}
    temp[#temp+1]=array[6]
    temp[#temp+1]=array[7]
    temp[#temp+1]=array[8]
    fight1:setFlyingPath(temp)
    action=cc.BezierTo:create(1,temp)
    fight1:setFighterState(Fighter.StateTargetFlying)

    local function callback2()
        --        fight1:removeSelf()
        --        fight1:setFighterState(Fighter.StateTargetAttackingInit)
        print("xxxxx")
    end
    fight1:runAction(cc.Sequence:create(action,cc.CallFunc:create(callback2)))


end

function BigFighter:spawFighter(data)
    
    local fight=Fighter.new(self.gameScene,self.x,self.y,data,self)
    self.gameScene:add2Team(fight)


end

function BigFighter:updateAutoSpawning(dt)
    
    
    
end

function BigFighter:stopAutoSpawning()
    
    if self.autoSpawAction~=nil then
        self:stopAction(self.autoSpawAction)
        self.autoSpawAction=nil
    end
    
end

function BigFighter:setAutoSpawning()
    
    local index=1
    
    local time=self:getSelectData(index).time
    local delay=cc.DelayTime:create(time)
    local function callback()
        self:spawFighter(self:getSelectData(index))
    end
    
    local seq=cc.Sequence:create(delay,cc.CallFunc:create(callback))
    self.autoSpawAction=cc.RepeatForever:create(seq)  
    self:runAction(self.autoSpawAction)
    
end


function BigFighter:getSelectData(index)
    
    return self.gameScene.data[index]

end

function BigFighter:hurt(bullet)


end

function BigFighter:updateFighter(dt)

    
--    self.curTime=self.curTime+dt;
--    if (self.curTime-self.lastSpawtime)>self.deltaTime then
--        self.lastSpawtime=self.curTime
--        self:spawFighter(dt)
--
--    end

end

function BigFighter:onEnter()


end

function BigFighter:onExit()


end

function BigFighter:onEnterTransitionFinish()


end

function BigFighter:onExitTransitionStart()


end

function BigFighter:onCleanup()


end


return BigFighter