AppConfig=
{
    defaultVsh="default_PositionTexture.vsh",
    defaultFsh="default_PositionTexture.vsh",

}

BulletCheckWidth=150
BulletCheckHeight=150

rowsHeight=150
rowsWidth=750

WindowScale=0.5

FriendPositions={
    
--        {rowsWidth/4,rowsHeight/2},
--        {rowsWidth/4*2,rowsHeight/2},
--        {rowsWidth/4*3,rowsHeight/2},
--        {rowsWidth/3,rowsHeight/2*3},
--        {rowsWidth/3*2,rowsHeight/2*3},
--        {rowsWidth/2,rowsHeight/2*5},
        {rowsWidth/12,rowsHeight/2},
        {rowsWidth/12*5,rowsHeight/2},
        {rowsWidth/12*9,rowsHeight/2},
        {rowsWidth/12*3,rowsHeight/2*3},
        {rowsWidth/12*7,rowsHeight/2*3},
        {rowsWidth/12*11,rowsHeight/2*3},
}

EnemyPositions={

        {rowsWidth/4,-rowsHeight/2},
        {rowsWidth/4*2,-rowsHeight/2},
        {rowsWidth/4*3,-rowsHeight/2},
        {rowsWidth/3,-rowsHeight/2*3},
        {rowsWidth/3*2,-rowsHeight/2*3},
        {rowsWidth/2,-rowsHeight/2*5},

}

GTeamFriend=1
GTeamEnemy=GTeamFriend+1

GFighterTypeBig=1
GFighterTypeMid=GFighterTypeBig+1
GFighterTypeSmall=GFighterTypeMid+1


GID=1













