
local ShaderNode=require("app.scenes.ShaderNode")

local BattleScene=require("app.ingame.BattleScene")
local TextureNode=require("app.ingame.TextureNode")



local MainScene = class("MainScene", function()
    return display.newScene("MainScene")

end)

function MainScene:ctor()

    local pbFilePath = cc.FileUtils:getInstance():fullPathForFilename("pbc/addressbook.pb")
    print("PB file path: "..pbFilePath)

    local pb = require "app.pbc.protobuf"
    local buffer = readProtobufFile(pbFilePath)

    pb.register(buffer)
    local stringbuffer = pb.encode("tutorial.Person",
        {
            name = "Alice",
            id = 12345,
            phone = {
                {
                    number = "87654321"
                },
            }
        })

    local slen = string.len(stringbuffer)
    local temp = ""
    for i=1, slen do
        temp = temp .. string.format("0xX, ", string.byte(stringbuffer, i))
    end
    print(temp)
    local result = pb.decode("tutorial.Person", stringbuffer)
    print("result name: "..result.name)


    --    local node=BattleScene.new()
    --    self:addChild(node)
    --    cc.uiloader:load("MainScene.csb"):addTo(self)


    --    local node=ShaderNode.new(display.width/2,display.height/2,512,512,"","galaxy.fsh")
    --    self.node=node
    --    self:addChild(node,10)
    --    self:addChild(createShaderMajoriTest(),10)
    --    cc.ui.UILabel.new({
    --            UILabelType = 2, text = "Hello,World ", size = 64})
    --        :align(display.CENTER, display.cx, display.cy)
    --        :addTo(self)
    --    self:addChild(TextureNode.new(self,256,500,256,0,30))

end

function MainScene:onEnter()
end

function MainScene:onExit()
end

return MainScene
