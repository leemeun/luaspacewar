
local ShaderNode = class("ShaderNode", function(x,y,width,height)
  
    local glNode  = gl.glNodeCreate()
    return glNode;
    
end)

function ShaderNode:ctor(x,y,width,height,vsh,fsh)
    local size = cc.size(width, height)
    self:setAnchorPoint(cc.p(0.5, 0.5))
    self:setContentSize(size)
    
    local scaleFactor = cc.Director:getInstance():getContentScaleFactor()
    local glProgram=self:loadShaderFile(vsh,fsh)
    local glState=cc.GLProgramState:getOrCreateWithGLProgram(glProgram);
    self:setGLProgramState(glState)
    local centerLoc = gl.getUniformLocation( glProgram:getProgram(), "center")
    local resolutionLoc = gl.getUniformLocation( glProgram:getProgram(), "resolution")
    
    self:setPosition( x, y)
    local center = cc.p( x * WindowScale*scaleFactor, y * WindowScale*scaleFactor)

    local resolution = cc.p(width* WindowScale*scaleFactor, height* WindowScale*scaleFactor)
    
    glState:setUniformVec2(centerLoc, center)
    glState:setUniformVec2(resolutionLoc, resolution)
--    glState:setUniformVec2("center", center)
--    glState:setUniformVec2("resolution", resolution)

    self.time=0
    
    local function updateShaderNode(fTime)
        self.time = self.time + fTime
    end
    
    local function drawShaderNode(transform, transformUpdated)
        local w = width*scaleFactor
        local h = height*scaleFactor
        local vertices ={ 0,0, w,0, w,h, 0,0, 0,h, w,h }
        local glProgramState = self:getGLProgramState()
        glProgramState:setVertexAttribPointer("a_position", 2, gl.FLOAT, false, 0, vertices, #vertices)
        glProgramState:apply(transform)
        gl.drawArrays(gl.TRIANGLES, 0, 6)
    end
    
    self:scheduleUpdateWithPriorityLua(updateShaderNode,0)
    self:registerScriptDrawHandler(drawShaderNode)
    
end

function ShaderNode:loadShaderFile(ver,frag)

    local vshString="";
    local fshString="";
    local pathVer=cc.FileUtils:getInstance():fullPathForFilename(ver);
    local pathFrag=cc.FileUtils:getInstance():fullPathForFilename(frag);
    vshString=cc.FileUtils:getInstance():getStringFromFile(ver);
    fshString=cc.FileUtils:getInstance():getStringFromFile(frag);
    if vshString==nil or vshString=="" then
        pathVer=cc.FileUtils:getInstance():fullPathForFilename(AppConfig.defaultVsh);
        vshString=cc.FileUtils:getInstance():getStringFromFile(pathVer);
    end
    if fshString==nil or fshString=="" then
        pathVer=cc.FileUtils:getInstance():fullPathForFilename(AppConfig.defaultFsh);
        fshString=cc.FileUtils:getInstance():getStringFromFile(pathFrag);
    end
    local glProgram=cc.GLProgram:createWithByteArrays(vshString,fshString);

    
    return glProgram;
    
end




function ShaderNode:onEnter()


end

function ShaderNode:onExit()


end

function ShaderNode:onEnterTransitionFinish()


end

function ShaderNode:onExitTransitionStart()


end

function ShaderNode:onCleanup()


end


return ShaderNode