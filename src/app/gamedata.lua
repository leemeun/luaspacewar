



FightersData=
    {
        {
            typeID="1",
            resID="1",
            hp="350",
            attack="30",
            defense="20",
            attrib="1",
            time="3",
            weapon="1",
            emitter="1",
            weaponType="1",
        },
        {
            typeID="2",
            resID="2",
            hp="450",
            attack="30",
            defense="20",
            attrib="1",
            time="5",
            weapon="1",
            emitter="1",
            weaponType="1",
        },
        {
            typeID="3",
            resID="3",
            hp="550",
            attack="30",
            defense="20",
            attrib="1",
            time="7",
            weapon="1",
            emitter="1",
            weaponType="1",
        },
        {
            typeID="4",
            resID="4",
            hp="650",
            attack="30",
            defense="20",
            attrib="1",
            time="8",
            weapon="1",
            emitter="1",
            weaponType="1",
        }

    }

weaponsData={

        {
            ID="1",
            type="1",
            attack="10",
            delaTime="0.5"
        },
        {
            ID="2",
            type="2",
            attack="10",
            delaTime="0.5"
        },
        {
            ID="3",
            type="3",
            attack="10",
            delaTime="0.5"
        },
}
