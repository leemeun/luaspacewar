
require("app.AppConfig")
require("app.common.Util")
require("app.gamedata")

local MyApp = class("MyApp")

local MainScene=require("app.scenes.MainScene")
local BattleScene=require("app.ingame.BattleScene")



function MyApp:ctor()
    math.randomseed(os.time())
end

function MyApp:run()

    cc.FileUtils:getInstance():addSearchPath("res/")
    cc.FileUtils:getInstance():addSearchPath("res/images")
    cc.FileUtils:getInstance():addSearchPath("res/shader")
    cc.FileUtils:getInstance():addSearchPath("res/shaderEffect")
    cc.FileUtils:getInstance():addSearchPath("res/ingame/fighters")
    cc.FileUtils:getInstance():addSearchPath("res/ingame/bullets")
    cc.FileUtils:getInstance():addSearchPath("res/ingame")
    cc.FileUtils:getInstance():addSearchPath("res/editor")
    cc.FileUtils:getInstance():addSearchPath("res/particle")
--    local scene=BattleScene.new(FightersData)
    local scene=MainScene.new()
    
    display.runScene(scene)
--    local label1 = cc.LabelTTF:create("Testing A8 Format", "Marker Felt", 48)
--    label1:setPosition(cc.p(display.width/2,display.height/2))
--    
--    scene:addChild(label1)
end


return MyApp
