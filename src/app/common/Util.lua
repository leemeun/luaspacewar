

function math.myMod(a,b)
    return  a - math.floor(a/b)*b
end


function getDistance(x0,y0,x1,y1)

    return math.sqrt((x0-x1)*(x0-x1)+(y0-y1)*(y0-y1))
end

function getDistanceByPoint(v1,v2)

    return cc.pGetDistance(v1,v2)
end


function math.myGetAngle(p)
    
    return cc.pGetAngle(cc.p(1,0),p)*180/math.pi
    
end


function create2DArray(row,col)
    local temp={}
    local i=0
    local j=0
    for i=0,row do
        temp[i]={}
        for j=0,col do
            temp[i][j]=0
        end
    end
    return temp
end






