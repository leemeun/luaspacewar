@echo off
set srcDir=D:\opensource\spacewar
set destDir=D:\opensource\war3.7

rmdir /s/q %destDir%\res
rmdir /s/q %destDir%\src\app
rmdir /s/q %destDir%\src\packages

del %destDir%\src\config.lua /q/f/s
del %destDir%\src\main.lua /q/f/s

copy %srcDir%\src\config.lua %destDir%\src
copy %srcDir%\src\main.lua %destDir%\src

python copy.py %srcDir%/res %destDir%/res
python copy.py %srcDir%/src/app %destDir%/src/app


