//
//  pbc-lua.h
//  pbc
//
//  Created by gamesoul-empirelegion on 15/9/9.
//  Copyright (c) 2015年 ztgame. All rights reserved.
//

#ifndef pbc_pbc_lua_h
#define pbc_pbc_lua_h


#if defined(_USRDLL)
#define LUA_EXTENSIONS_DLL     __declspec(dllexport)
#else         /* use a DLL library */
#define LUA_EXTENSIONS_DLL
#endif

#if __cplusplus
extern "C" {
#endif
    
#include "lauxlib.h"
    
    int LUA_EXTENSIONS_DLL luaopen_protobuf_c(lua_State *L);
    
#if __cplusplus
}
#endif


#endif
