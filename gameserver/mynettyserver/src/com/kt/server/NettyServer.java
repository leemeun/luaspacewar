package com.kt.server;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ChannelFactory;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;

import java.net.InetSocketAddress;

import org.apache.log4j.Logger;






public class NettyServer {

	
	
	private static final Logger logger = Logger.getLogger(NettyServer.class);
	private final int port;
	
	public NettyServer(int port)
	{
		this.port=port;
	}
	
	void startServer() throws Exception
	{
		EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap b = new ServerBootstrap();
            
            b.group(bossGroup, workerGroup)
             .channel(NioServerSocketChannel.class)
             .childOption(ChannelOption.TCP_NODELAY, true)
             .childOption(ChannelOption.SO_KEEPALIVE, true)
             .childOption(ChannelOption.SO_REUSEADDR, true) //���õ�ַ
             .childOption(ChannelOption.SO_RCVBUF, 1048576)
             .childOption(ChannelOption.SO_SNDBUF, 1048576)
             .childOption(ChannelOption.ALLOCATOR, new PooledByteBufAllocator(false))  // heap buf 's better
             .childHandler(new ServerChannelInitializer());

            ChannelFuture future = b.bind(port).sync();
            logger.info("Server started on port [" + this.port + "]");
//
//            SocketManager.schedule(new HeartbeatTimer(), Config.HEARTBEAT_INTERVAL);
//            logger.info("Scheduled HeartbeatTimer ...");
//
//            SocketManager.schedule(new LayoutTimer(), Config.LAYOUT_TIMEOVER);
//            logger.info("Scheduled LayoutTimer ...");
//
//            SocketManager.schedule(new AttackTimer(), Config.ATTACK_TIMEOVER);
//            logger.info("Scheduled AttackTimer ...");

            future.channel().closeFuture().sync();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
	}
	
	void stopServer()
	{
		
	}
	
	public static void main(String[] args) {
		
		NettyServer server=new NettyServer(8010);
		try {
			server.startServer();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
}
