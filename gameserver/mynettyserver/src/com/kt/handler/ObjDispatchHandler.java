package com.kt.handler;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.kt.protobean.AddressBookProtos.AddressBook;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class ObjDispatchHandler extends ChannelInboundHandlerAdapter {

	private Logger logger=Logger.getLogger(ObjDispatchHandler.class.getName());
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		AddressBook adb=(AddressBook)msg;
		logger.log(Level.INFO, adb.toString());
		
	}
	
	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		
		
	}
	
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		
		logger.log(Level.INFO, "AuthServerInitHandler exceptionCaught");
		ctx.close();
	}
	
}
