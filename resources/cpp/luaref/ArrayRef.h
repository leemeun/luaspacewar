//
//  ArrayRef.h
//  war3.7
//
//  Created by gamesoul-empirelegion on 15/8/11.
//
//

#ifndef __war3_7__ArrayRef__
#define __war3_7__ArrayRef__

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;



class CC_DLL  ArrayRef : public Ref
{
    float *array=NULL;
    
public:
    
    static ArrayRef* create();
    
	void setArray(float* arr)
    {
        array=arr;
    }
    
    ArrayRef();
    
    ~ArrayRef()
    {
        if(array!=NULL)
        {
            delete array;
        }
    }
    
    
};



#endif /* defined(__war3_7__ArrayRef__) */
