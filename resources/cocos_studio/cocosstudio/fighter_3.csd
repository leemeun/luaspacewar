<GameProjectFile>
  <PropertyGroup Type="Node" Name="fighter3" ID="41652244-168e-4d79-aadd-5e25a8c38a37" Version="2.2.9.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="6" ctype="GameNodeObjectData">
        <Size />
        <Children>
          <AbstractNodeData Name="fire1" ActionTag="-228170623" Tag="1" IconVisible="True" LeftMargin="-26.2197" RightMargin="26.2197" TopMargin="-45.3265" BottomMargin="45.3265" ctype="SingleNodeObjectData">
            <Size />
            <AnchorPoint />
            <Position X="-26.2197" Y="45.3265" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize />
          </AbstractNodeData>
          <AbstractNodeData Name="fire2" ActionTag="893700508" Tag="2" IconVisible="True" LeftMargin="24.8939" RightMargin="-24.8939" TopMargin="-45.5244" BottomMargin="45.5244" ctype="SingleNodeObjectData">
            <Size />
            <AnchorPoint />
            <Position X="24.8939" Y="45.5244" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize />
          </AbstractNodeData>
          <AbstractNodeData Name="emitter1" ActionTag="-1903677117" Tag="13" IconVisible="True" LeftMargin="-22.6437" RightMargin="22.6437" TopMargin="31.0609" BottomMargin="-31.0609" ctype="SingleNodeObjectData">
            <Size />
            <AnchorPoint />
            <Position X="-22.6437" Y="-31.0609" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize />
          </AbstractNodeData>
          <AbstractNodeData Name="emitter2" ActionTag="459863556" Tag="14" IconVisible="True" LeftMargin="22.3792" RightMargin="-22.3792" TopMargin="31.9264" BottomMargin="-31.9264" ctype="SingleNodeObjectData">
            <Size />
            <AnchorPoint />
            <Position X="22.3792" Y="-31.9264" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize />
          </AbstractNodeData>
          <AbstractNodeData Name="sprite" ActionTag="-21635769" Tag="7" IconVisible="False" LeftMargin="-51.0000" RightMargin="-51.0000" TopMargin="-50.5000" BottomMargin="-50.5000" ctype="SpriteObjectData">
            <Size X="102.0000" Y="101.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize />
            <FileData Type="Normal" Path="battleres/fighter3.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>