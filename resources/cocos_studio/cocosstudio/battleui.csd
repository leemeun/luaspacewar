<GameProjectFile>
  <PropertyGroup Type="Layer" Name="battleui" ID="bdf8f9e1-b21f-42a6-8d8c-5818f812e7c6" Version="2.2.9.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="11" ctype="GameLayerObjectData">
        <Size X="750.0000" Y="1334.0000" />
        <Children>
          <AbstractNodeData Name="but_fighter1" ActionTag="-1775587163" CallBackType="Click" Tag="18" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="62.1791" RightMargin="559.8209" TopMargin="1203.0000" BottomMargin="3.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="76" Scale9Height="74" ctype="ButtonObjectData">
            <Size X="128.0000" Y="128.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="126.1791" Y="67.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1682" Y="0.0502" />
            <PreSize X="0.1707" Y="0.0960" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="res/but_fighter1.png" Plist="" />
            <NormalFileData Type="Normal" Path="res/but_fighter1.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="but_fighter2" ActionTag="2088866381" Tag="9" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="227.6969" RightMargin="394.3031" TopMargin="1203.0284" BottomMargin="2.9716" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" Scale9Width="76" Scale9Height="73" ctype="ButtonObjectData">
            <Size X="128.0000" Y="128.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="291.6969" Y="66.9716" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3889" Y="0.0502" />
            <PreSize X="0.1707" Y="0.0960" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="res/but_fighter2.png" Plist="" />
            <NormalFileData Type="Normal" Path="res/but_fighter2.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="but_fighter3" ActionTag="-2044104587" Tag="11" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="407.0094" RightMargin="214.9906" TopMargin="1199.0404" BottomMargin="6.9596" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" Scale9Width="76" Scale9Height="74" ctype="ButtonObjectData">
            <Size X="128.0000" Y="128.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="471.0094" Y="70.9596" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6280" Y="0.0532" />
            <PreSize X="0.1707" Y="0.0960" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="res/but_fighter3.png" Plist="" />
            <NormalFileData Type="Normal" Path="res/but_fighter3.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="but_fighter4" ActionTag="1294278906" Tag="12" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="584.2518" RightMargin="37.7482" TopMargin="1199.5137" BottomMargin="6.4863" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" Scale9Width="76" Scale9Height="74" ctype="ButtonObjectData">
            <Size X="128.0000" Y="128.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="648.2518" Y="70.4863" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8643" Y="0.0528" />
            <PreSize X="0.1707" Y="0.0960" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="res/but_fighter4.png" Plist="" />
            <NormalFileData Type="Normal" Path="res/but_fighter4.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>