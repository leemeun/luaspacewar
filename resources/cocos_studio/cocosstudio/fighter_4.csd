<GameProjectFile>
  <PropertyGroup Type="Node" Name="fighter4" ID="ec356487-05b5-4155-ab47-2a3c2a5dfa0b" Version="2.2.9.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="6" ctype="GameNodeObjectData">
        <Size />
        <Children>
          <AbstractNodeData Name="fire1" ActionTag="-228170623" Tag="1" IconVisible="True" LeftMargin="-24.7503" RightMargin="24.7503" TopMargin="-31.3708" BottomMargin="31.3708" ctype="SingleNodeObjectData">
            <Size />
            <AnchorPoint />
            <Position X="-24.7503" Y="31.3708" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize />
          </AbstractNodeData>
          <AbstractNodeData Name="fire2" ActionTag="893700508" Tag="2" IconVisible="True" LeftMargin="21.9567" RightMargin="-21.9567" TopMargin="-32.3034" BottomMargin="32.3034" ctype="SingleNodeObjectData">
            <Size />
            <AnchorPoint />
            <Position X="21.9567" Y="32.3034" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize />
          </AbstractNodeData>
          <AbstractNodeData Name="emitter1" ActionTag="-1903677117" Tag="13" IconVisible="True" LeftMargin="-24.5101" RightMargin="24.5101" TopMargin="42.2525" BottomMargin="-42.2525" ctype="SingleNodeObjectData">
            <Size />
            <AnchorPoint />
            <Position X="-24.5101" Y="-42.2525" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize />
          </AbstractNodeData>
          <AbstractNodeData Name="emitter2" ActionTag="459863556" Tag="14" IconVisible="True" LeftMargin="22.3811" RightMargin="-22.3811" TopMargin="41.2523" BottomMargin="-41.2523" ctype="SingleNodeObjectData">
            <Size />
            <AnchorPoint />
            <Position X="22.3811" Y="-41.2523" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize />
          </AbstractNodeData>
          <AbstractNodeData Name="sprite" ActionTag="-21635769" Tag="7" IconVisible="False" LeftMargin="-54.5000" RightMargin="-54.5000" TopMargin="-56.5000" BottomMargin="-56.5000" ctype="SpriteObjectData">
            <Size X="109.0000" Y="113.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize />
            <FileData Type="Normal" Path="battleres/fighter4.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>