<GameProjectFile>
  <PropertyGroup Type="Node" Name="fighter2" ID="b0f7cf64-7d7b-4d5a-9a64-5fa247b14e2b" Version="2.2.9.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="10" ctype="GameNodeObjectData">
        <Size />
        <Children>
          <AbstractNodeData Name="fire1" ActionTag="-1024875063" Tag="1" IconVisible="True" LeftMargin="-24.5613" RightMargin="24.5613" TopMargin="-36.6742" BottomMargin="36.6742" ctype="SingleNodeObjectData">
            <Size />
            <AnchorPoint />
            <Position X="-24.5613" Y="36.6742" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize />
          </AbstractNodeData>
          <AbstractNodeData Name="fire2" ActionTag="-1791798894" Tag="2" IconVisible="True" LeftMargin="14.3032" RightMargin="-14.3032" TopMargin="-36.6742" BottomMargin="36.6742" ctype="SingleNodeObjectData">
            <Size />
            <AnchorPoint />
            <Position X="14.3032" Y="36.6742" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize />
          </AbstractNodeData>
          <AbstractNodeData Name="emitter1" ActionTag="1358121696" Tag="11" IconVisible="True" LeftMargin="-22.6558" RightMargin="22.6558" TopMargin="33.6459" BottomMargin="-33.6459" ctype="SingleNodeObjectData">
            <Size />
            <AnchorPoint />
            <Position X="-22.6558" Y="-33.6459" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize />
          </AbstractNodeData>
          <AbstractNodeData Name="emitter2" ActionTag="-1095662330" Tag="12" IconVisible="True" LeftMargin="11.3987" RightMargin="-11.3987" TopMargin="32.1178" BottomMargin="-32.1178" ctype="SingleNodeObjectData">
            <Size />
            <AnchorPoint />
            <Position X="11.3987" Y="-32.1178" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize />
          </AbstractNodeData>
          <AbstractNodeData Name="fighter2" ActionTag="1178466253" Tag="12" IconVisible="False" LeftMargin="-65.5000" RightMargin="-53.5000" TopMargin="-43.5000" BottomMargin="-43.5000" ctype="SpriteObjectData">
            <Size X="119.0000" Y="87.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-6.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize />
            <FileData Type="Normal" Path="battleres/fighter2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>