

srcDir=/Users/empreefleet/game/spacewar
destDir=/Users/empreefleet/game/war3.7


rm -rf $destDir/res
rm -rf $destDir/src/app
rm -rf $destDir/src/packages


rm $destDir/src/config.lua
rm $destDir/src/main.lua

cp $srcDir/src/config.lua $destDir/src
cp $srcDir/src/main.lua $destDir/src
cp $srcDir/config.json $destDir

python copy.py $srcDir/res $destDir/res
python copy.py $srcDir/src/app $destDir/src/app


