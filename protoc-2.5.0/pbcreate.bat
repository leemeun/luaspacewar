@echo off
set DIR=%~dp0
cd /d "%DIR%"
setlocal enabledelayedexpansion
for /r %%i in (*.proto) do ( 
set pbname=%%i 
      set pbname=!pbname:~0,-5!b
      @echo %DIR%
      @echo %%i
      @echo !pbname!
      protoc -I %DIR% --descriptor_set_out !pbname! %%i 
      protoc -I %DIR% --cpp_out %DIR% %%i
)
pause